# Weird gotchas

```gradle
// Kad veiktu MAPS ant Android
// node_modules/react-native-maps/lib/android/build.gradle
dependencies {
  def googlePlayServicesVersion = rootProject.hasProperty('googlePlayServicesVersion')  ? rootProject.googlePlayServicesVersion : DEFAULT_GOOGLE_PLAY_SERVICES_VERSION
  def androidMapsUtilsVersion   = rootProject.hasProperty('androidMapsUtilsVersion')    ? rootProject.androidMapsUtilsVersion   : DEFAULT_ANDROID_MAPS_UTILS_VERSION

  compile "com.facebook.react:react-native:+"
  compile "com.google.android.gms:play-services-base:$googlePlayServicesVersion"
  compile "com.google.android.gms:play-services-maps:$googlePlayServicesVersion"
  compile "com.google.maps.android:android-maps-utils:$androidMapsUtilsVersion"
}
```

* Google API Keys
  * iOS: `AIzaSyAG4oijQPdfPL7psf7wPhfyuIjDPJN4CoA`
  * Android: `AIzaSyDxin9KVyjkLB0ptkwZJ44NqhQW6U2TyzM`

`src` - Aplikacijos source kodas

`src/actions` - Redux veiksmai globalioms aplikacijos operacijoms vykdyti

`src/pages` - Aplikacijos puslapiai.

`src/components` - React komponentai

`src/reducers` - Redux reducer funkcijos, apjungiančios `/actions` kataloge esančius veiksmus

`src/start`

---

# Setup

Parsisiuntus projektą, `android` folderyje reikia:

1.  `app/build.gradle`, `android/settings.gradle`, `java/.../botanika/MainApplication.java` failuose ištrinti visus paminėtus paketus išskyrus `react-native-sqlite-storage`

    * `app/build.gradle`

    ```gradle
     ...
     dependencies {
         // -- PALIEKAM TIK ŠIUOS ĮRAŠUS --
         compile project(':react-native-sqlite-storage')
         compile fileTree(dir: "libs", include: ["*.jar"])
         compile "com.android.support:appcompat-v7:23.0.1"
         compile "com.facebook.react:react-native:+"  // From node_modules
     }
     ...
    ```

    * `android/settings.gradle`


    ```gradle
    rootProject.name = 'botanika'
    // tik šituos paliekam
    include ':react-native-sqlite-storage'
    project(':react-native-sqlite-storage').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-sqlite-storage/src/android')

    include ':app'
    ```

    * `java/.../botanika/MainApplication.java`


    ```java
    package com.botanika;

    import android.app.Application;
    import org.pgsqlite.SQLitePluginPackage; // paliekam šitą
    // TRINAM VISKA IKI...
    import com.facebook.react.ReactApplication;
    import com.facebook.react.ReactNativeHost;
    import com.facebook.react.ReactPackage;
    ...
        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                new MainReactPackage(),
                    new SQLitePluginPackage()   // palikti
                    // trinti visa kita
            );
        }
    ```

2.  Ištriname `~/android/app/build/` ir `node_modules` aplanką su

    Windows

    ```bat
    cd android/app/ && rmdir "build/" /S /Q && cd ../.. && rmdir "node_modules/" /S /Q
    ```

    Mac (Linux)

    ```bat
    cd android/app/ && rm -rf build/ && cd ../.. && rm -rf node_modules/
    ```

3.  Įrašome visus trūkstamus paketus su

    NPM

    ```bat
    npm install
    ```

    Yarn (rekomenduojama)

    ```bat
    yarn install
    ```

4.  Surišame visus reikiamus plug-in'us su `react-native link <paketas>` komanda:
    ```bat
    react-native link react-native-fetch-blob
    react-native link react-native-fs
    react-native link react-native-gesture-handler
    react-native link react-native-linear-gradient
    react-native link react-native-tab-view
    react-native link react-native-vector-icons
    react-native link react-native-zip-archive
    ```
5.  Paleidžiame norimą platformą su `react-native run-ios` arba `react-native run-android` komanda.

---

## Samples

#### Gets data from remote server, updates it with a few rows and, finally, uploads it back to the server

```js
xDB
  .fetch()
  .then(_ =>
    xDB.query("INSERT INTO Objects VALUES (?,?,?,?,?,?,?,?,?,?)", [
      null,
      2,
      "TESTINIS TIPAS",
      "TESTINIS PAVADINIMAS",
      "11.11111",
      "22.22222",
      "TESTINIS APRAŠYMAS",
      "777",
      null,
      null
    ])
  )
  .then(_ => {
    xDB.push();
  });
```

---

## Native kodo niuansai

Leidimai:
`/android/app/src/main/AndroidManifest.xml`

```xml
<!-- FAILAMS PARSISIŲSTI -->
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
<!-- PATIKRINIMUI AR YRA INTERNETAS -->
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```
