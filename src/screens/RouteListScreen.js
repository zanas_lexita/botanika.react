import React, { Component } from "react";
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from "react-native";
import { withNavigation, NavigationScreenProps } from "react-navigation";
import { connect } from "react-redux";
import { TEXT, COLORS } from "../globals";
import { ObjectImage } from "../components";
import { coordDistance, humanDistance } from "../plugins/Utilities";
import * as actions from "../redux/actions";

const styles = StyleSheet.create({});

type RouteListScreenProps = NavigationScreenProps & {};
class RouteListScreen extends Component<RouteListScreenProps> {
  constructor(props) {
    super(props);
    this.state = {};
    this.renderItem = this.renderItem.bind(this);
  }

  renderItem(entry) {
    const { lang, navStack } = this.props;
    const {
      Name,
      distance,
      objects,
      Latitude,
      Longitude,
      MainPhoto
    } = entry.item;

    const sub = lang.objectCount + ": " + objects.length + " | " + distance;

    return (
      <TouchableHighlight
        underlayColor="rgba(0, 0, 0, 0.06)"
        onPress={() => navStack.navigate("Route", entry.item)}
      >
        <ObjectImage
          style={{ borderRadius: 8, margin: 8, overflow: "hidden" }}
          icon="dot-circle-o"
          src={MainPhoto}
          title={Name}
          sub={sub}
        />
      </TouchableHighlight>
    );
  }

  render() {
    const { location, routes } = this.props;

    return (
      <FlatList
        data={routes}
        extraData={location}
        renderItem={this.renderItem}
        keyExtractor={item => item.RouteId.toString()}
      />
    );
  }
}

const mapStateToProps = state => ({
  lang: state.language.lang,
  order: state.order.order,
  routes: state.fetch.routes,
  location: state.location.location
});

export default connect(mapStateToProps, actions)(
  withNavigation(RouteListScreen)
);
