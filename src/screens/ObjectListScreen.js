import React, { Component } from "react";
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View
} from "react-native";
import Collapsible from "react-native-collapsible";
import { NavigationScreenProps } from "react-navigation";
import { connect } from "react-redux";
import { Icon, InlineSearch, XButton } from "../components";
import { coordDistance, humanDistance, searchify } from "../plugins/Utilities";
import * as actions from "../redux/actions";
import { ORDER_BY_LOCATION, ORDER_BY_NAME } from "../redux/actions/types";
import { COLORS, TEXT } from "./../globals";

const styles = StyleSheet.create({
  filterMessageView: {
    alignItems: "center",
    flex: 0,
    flexDirection: "row",
    marginBottom: 16,
    marginTop: 8,
    paddingHorizontal: 16
  },
  filterAction: {
    paddingHorizontal: 8,
    paddingVertical: 4,
    flex: 0
  },
  filterMessage: {
    color: COLORS.text.helper,
    fontSize: 11,
    marginRight: "auto",
    position: "absolute",
    top: 46,
    right: 16
  },
  item: {
    alignItems: "center",
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 16,
    paddingVertical: 8
  },
  sub: {
    color: COLORS.text.secondary
  },
  image: {
    borderRadius: 25,
    marginRight: 16,
    height: 50,
    width: 50
  }
});

class ObjectListActions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filtersClosed: true,
      selected: null
    };

    this.toggleFilters = this.toggleFilters.bind(this);
  }

  toggleFilters() {
    const nextState = !this.state.filtersClosed;

    this.setState({
      ...this.state,
      filtersClosed: nextState,
      selected: nextState ? this.state.selected : null
    });

    if (nextState) {
      this._onFilter();
    }
  }

  _onSort() {
    const { onSort } = this.props;
    if (typeof onSort === "function") onSort();
  }

  _onFilter(val) {
    const { onFilter } = this.props;
    if (typeof onFilter === "function") onFilter(val);
  }

  _onSearch(val) {
    const { onSearch } = this.props;
    if (typeof onSearch === "function") onSearch(val);
  }

  render() {
    const { sortMsg, filterTypes } = this.props;
    const { selected, filtersClosed } = this.state;

    const FiltersView = ({ style }) => (
      <View
        style={[
          {
            flexDirection: "row",
            justifyContent: "center",
            flexWrap: "wrap"
          },
          style
        ]}
      >
        {filterTypes.map(ft => (
          <XButton
            key={ft.OptionDetailId}
            onPress={() => {
              const next =
                selected === ft.OptionDetailId ? null : ft.OptionDetailId;

              this._onFilter(next);
              this.setState({ ...this.state, selected: next });
            }}
            title={ft.Name}
            primary={ft.OptionDetailId === selected ? true : false}
            rounded
          />
        ))}
      </View>
    );

    return (
      <View>
        <Collapsible collapsed={filtersClosed}>
          <FiltersView style={{ marginTop: 8, marginBottom: -8 }} />
        </Collapsible>
        <View style={styles.filterMessageView}>
          <View style={{ flex: 1 }}>
            <InlineSearch
              open
              icon={false}
              onSearch={val => this._onSearch(val)}
              style={{ alignSelf: "flex-end" }}
            />
          </View>
          <TouchableOpacity
            style={styles.filterAction}
            onPress={this.toggleFilters}
          >
            <Icon color={COLORS.text.secondary} size={18} name="filter" />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.filterAction}
            onPress={() => this._onSort()}
          >
            <Icon color={COLORS.text.secondary} size={24} name="sort" />
          </TouchableOpacity>
          <Text style={styles.filterMessage}>{sortMsg}</Text>
        </View>
      </View>
    );
  }
}

type ObjectListScreenProps = NavigationScreenProps & {};
class ObjectListScreen extends Component<ObjectListScreenProps> {
  constructor(props) {
    super(props);

    this.state = {
      objects: props.objects || [],
      currentObjects: [],
      myLocation: null,
      search: "",
      filterType: null
    };

    this.renderItem = this.renderItem.bind(this);
  }

  itemsSort(objects) {
    const { location, order } = this.props;

    let sorted;
    switch (order) {
      case ORDER_BY_LOCATION:
        sorted = objects.sort((a, b) => {
          const distanceA = coordDistance(location, {
            lat: a.Latitude,
            lng: a.Longitude
          });

          const distanceB = coordDistance(location, {
            lat: b.Latitude,
            lng: b.Longitude
          });

          return distanceA - distanceB;
        });
        break;

      case ORDER_BY_NAME:
        sorted = objects.sort((a, b) => {
          return Number(a.Name > b.Name);
        });
        break;

      default:
        sorted = objects;
    }

    return sorted;
  }

  itemsFilter(objects) {
    const { filterType } = this.state;

    if (!filterType) return objects;

    return objects.filter(o => o.TypeId === filterType);
  }

  itemsSearch(objects) {
    const { search } = this.state;
    if (search.trim() === "") return objects;

    return objects.filter(
      o => searchify(o.Name).indexOf(searchify(search)) !== -1
    );
  }

  renderItem(entry) {
    const { Name, Latitude, Longitude, MainPhoto, TypeName } = entry.item;

    const coords = {
      lat: Latitude,
      lng: Longitude
    };

    const distance = humanDistance(coordDistance(this.props.location, coords));

    const sub = [distance, TypeName].filter(e => e).join(" | ");

    return (
      <TouchableHighlight
        underlayColor="rgba(0, 0, 0, 0.06)"
        onPress={() => this.props.navigation.navigate("Object", entry.item)}
      >
        <View style={styles.item}>
          <Image style={styles.image} source={{ uri: MainPhoto }} />
          <View>
            <Text style={TEXT.primary}>{Name}</Text>
            <Text style={[styles.sub, TEXT.secondary]}>{sub}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    const { location, order, lang, objects, types } = this.props;

    const sorted = this.itemsSort(objects);
    const filtered = this.itemsFilter(sorted);
    const searched = this.itemsSearch(filtered);

    const sortMsg = lang.orderBy + ": " + lang[order];

    const ListHeader = (
      <ObjectListActions
        filterTypes={types}
        onFilter={filterType => this.setState({ ...this.state, filterType })}
        onSearch={term => this.setState({ ...this.state, search: term })}
        sortMsg={sortMsg}
        onSort={() => this.props.setOrder()}
      />
    );

    return (
      <FlatList
        data={searched}
        extraData={location}
        renderItem={this.renderItem}
        ListHeaderComponent={ListHeader}
        ItemSeparatorComponent={({ highlighted }) => (
          <View
            style={{
              height: 1,
              marginLeft: 80,
              marginTop: -1,
              backgroundColor: COLORS.text.divider
            }}
          />
        )}
        keyExtractor={item => item.ObjectId.toString()}
      />
    );
  }
}

const mapStateToProps = state => ({
  lang: state.language.lang,
  locale: state.language.locale,
  order: state.order.order,
  objects: state.fetch.objects,
  types: state.fetch.types,
  location: state.location.location
});

export default connect(mapStateToProps, actions)(ObjectListScreen);
