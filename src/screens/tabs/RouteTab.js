import React from "react";
import { Text } from "react-native";

import { RouteListScreen } from "../";

export default ({ screenProps }) => (
  <RouteListScreen navStack={screenProps.navStack} />
);
