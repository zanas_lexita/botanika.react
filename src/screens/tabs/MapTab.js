import React, { Component } from "react";
import { ActivityIndicator, View } from "react-native";
import { connect } from "react-redux";
import { NavigationScreenProps, withNavigation } from "react-navigation";
import { calculateBounds } from "../../plugins/Utilities";
import { NavigationService } from "../../navigators";
import { ActionButton, XButton } from "./../../components";
import { XMap } from "./../../components";
import { COLORS } from "./../../globals";

class MapTab extends Component<NavigationScreenProps> {
  constructor(props) {
    super(props);

    // this.triggerLocate = this.triggerLocate.bind(this);
  }

  render() {
    const { screenProps, lang } = this.props;
    const { objects, gardenInfo, navStack, navSwitch } = screenProps;

    const region = calculateBounds(
      gardenInfo.BoundLat1,
      gardenInfo.BoundLat2,
      gardenInfo.BoundLng1,
      gardenInfo.BoundLng2
    );

    const loadedProps = region
      ? {
          // initialRegion: region,
          // region: region,
          places: objects
        }
      : false;

    return loadedProps ? (
      <View style={{ backgroundColor: "#f2f2f2", flex: 1 }}>
        <XButton
          onPress={() => {
            navSwitch.navigate("Setup");
          }}
          raised
          title={lang.changeGarden}
        />
        <XMap
          onRef={ref => {
            this.map = ref;
          }}
          minZoomLevel={10}
          maxZoomLevel={17}
          navStack={navStack}
          {...loadedProps}
          showsScale
          showsCompass
        />
        <ActionButton
          onPress={() => navStack.navigate("List")}
          icon="view_list"
        />
        <ActionButton
          onPress={() => {
            this.map.locate();
          }}
          icon="street-view"
          iconColor={COLORS.text.secondary}
          style={{ bottom: 72, backgroundColor: "#fff" }}
        />
      </View>
    ) : (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  lang: state.language.lang
});

export default connect(mapStateToProps)(withNavigation(withNavigation(MapTab)));
