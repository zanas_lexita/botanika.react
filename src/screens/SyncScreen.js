// @flow

import React, { Component } from "react";
import { View } from "react-native";
import { NavigationScreenProps, withNavigation } from "react-navigation";
import { connect } from "react-redux";
import * as actions from "../redux/actions";
import { DIRECTORIES } from "../globals";
import { checkConnection } from "../plugins/checkConnection";
import { syncFiles } from "../plugins/syncFiles";
import { ProgressBar } from "./../components/ProgressBar";
import { humanFilesize } from "./../plugins/Utilities";

type Props = NavigationScreenProps & {
  language: string
};

type State = {
  bytesToSync: number,
  bytesDownloaded: number
};

class SyncScreen extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      bytesToSync: 0,
      bytesDownloaded: 0
    };
  }

  componentDidMount() {
    const { garden, getData, navigation } = this.props;
    /**
     * 2. TINKLO SITUACIJŲ HANDLINIMAS
     */

    // Neturi interneto ir reikalingų failų
    const fullyOffline = () => {
      console.log("OFFLINE | No files");
    };

    // Paleidžiam pradinį vaizdą.
    const startApp = () => {
      getData().then(() => navigation.navigate("App"));
    };

    /**
     * 1. SITUACIJOS TIKRINIMAS
     */

    checkConnection(
      () => {
        // Fully Online
        syncFiles(
          DIRECTORIES.remoteRoot + "md5.php?sid=" + garden,
          (progress, total) => {
            // On progress update...
            this.setState({
              ...this.state,
              bytesDownloaded: progress,
              bytesToSync: total
            });
          },
          undefined,
          true
        )
          .then(_ => startApp())
          .catch(_ => navigation.navigate("NoNetwork"));
      },
      () => {
        // Synced, but offline
        startApp();
      },
      () => {
        // Fully Offline
        fullyOffline();
      }
    );
  }

  render() {
    const value = humanFilesize(this.state.bytesDownloaded);
    const outOf = humanFilesize(this.state.bytesToSync);
    let title = `${value} / ${outOf}`;

    if (this.state.bytesToSync === 0) {
      title = "";
    }

    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        {
          <ProgressBar
            title={title}
            value={this.state.bytesDownloaded}
            end={this.state.bytesToSync}
            style={{ width: 164 }}
          />
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  garden: state.garden.garden
});

// Connect everything
export default connect(mapStateToProps, actions)(withNavigation(SyncScreen));
