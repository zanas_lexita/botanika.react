import React from "react";
import { View, Text } from "react-native";
import { connect } from "react-redux";
import { XButton } from "../components";

const NoNetworkScreen = ({ lang, navigation }) => (
  <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
    <Text>{lang.noNetwork}</Text>
    <XButton
      title={lang.tryAgain}
      onPress={() => navigation.navigate("Sync")}
    />
  </View>
);

const mapStateToProps = state => ({
  lang: state.language.lang
});

export default connect(mapStateToProps)(NoNetworkScreen);
