import React from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import { NavigationScreenProps } from "react-navigation";

import { XButton, ObjectImage, AudioButton } from "./../components";
import { DICTIONARY } from "../plugins/translations";
import { GardenObject } from "./../plugins/Types";
import { TEXT } from "../globals";

const styles = StyleSheet.create({
  content: {
    padding: 16
  },
  description: {
    marginBottom: 16
  }
});

type ObjectScreenProps = NavigationScreenProps & {};
class ObjectScreen extends React.Component<ObjectScreenProps> {
  render() {
    const { lang, navigation } = this.props;

    const item: GardenObject = navigation.state.params;
    const {
      Name,
      TypeName,
      TypeIcon,
      Description,
      MainAudio,
      MainPhoto
    } = item;

    return (
      <ScrollView>
        <ObjectImage
          src={MainPhoto}
          title={Name}
          sub={TypeName}
          icon={TypeIcon}
        />
        <View style={styles.content}>
          <View style={{ flexDirection: "row", marginBottom: 8 }}>
            <XButton
              title={lang.goBack}
              icon="arrow_back"
              onPress={() => {
                navigation.goBack();
              }}
            />
          </View>
          <Text style={[styles.description, TEXT.primary]}>{Description}</Text>
          {MainAudio ? (
            <AudioButton audioName={Name} audio={MainAudio} />
          ) : (
            <View />
          )}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  lang: state.language.lang
});

export default connect(mapStateToProps)(ObjectScreen);
