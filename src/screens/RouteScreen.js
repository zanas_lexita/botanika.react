import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { connect } from "react-redux";
import { TEXT } from "../globals";
import { AudioButton, ObjectImage, XButton, XMapRoute } from "./../components";
import { GardenObject } from "./../plugins/Types";

type RouteScreenProps = NavigationScreenProps & {};
class RouteScreen extends React.Component<RouteScreenProps> {
  render() {
    const { lang, navigation } = this.props;

    const item: GardenObject = navigation.state.params;
    const { Name, Description, objects, TypeIcon, MainPhoto } = item;

    return (
      <ScrollView>
        <XMapRoute route={objects} />
        <View style={styles.content}>
          <View style={{ flexDirection: "row", marginBottom: 8 }}>
            <XButton
              title={lang.goBack}
              icon="arrow_back"
              onPress={() => {
                navigation.goBack();
              }}
            />
          </View>
          <Text style={[styles.description, TEXT.primary]}>{Description}</Text>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    padding: 16
  },
  description: {
    marginBottom: 16
  }
});

const mapStateToProps = state => ({
  lang: state.language.lang
});

export default connect(mapStateToProps)(RouteScreen);
