import React from "react";
import { ActivityIndicator, Platform, StyleSheet, View } from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { connect } from "react-redux";
import { checkConnection } from "../plugins/checkConnection";
import { syncFiles } from "../plugins/syncFiles";
import * as actions from "../redux/actions";
import { getGarden } from "../redux/actions/setGarden";
import { DEBUG, DEBUG_OPTIONS } from "./../globals";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});

type Props = NavigationScreenProps & {};
class LoadingScreen extends React.Component<Props> {
  componentDidMount() {
    const { getData, navigation } = this.props;
    // this.bootstrapAsync();
    const noData = () => {
      navigation.navigate("NoNetwork");
    };

    const hasData = online => {
      getGarden().then(garden => {
        if (DEBUG === true && DEBUG_OPTIONS.START === true) {
          garden = null;
        }

        const openApp = () => {
          const isSetup = garden === null;

          if (isSetup) {
            navigation.navigate("Setup");
          } else {
            getData().then(() => {
              navigation.navigate("App");
            });
          }
        };

        // Download database
        if (online) {
          syncFiles()
            .then(() => openApp())
            .catch(() => noData());
        } else {
          openApp();
        }
      });
    };

    // Get current garden
    checkConnection(() => hasData(true), () => hasData(false), noData);
  }

  render() {
    const loaderColor = "#ccc";
    const loaderSize = Platform.select({
      ios: "large",
      android: 48
    });

    return (
      <View style={styles.container}>
        <ActivityIndicator color={loaderColor} size={loaderSize} />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    lang: state.language.lang,
    locale: state.language.locale,
    garden: state.garden.garden
  };
};

export default connect(mapStateToProps, actions)(LoadingScreen);
