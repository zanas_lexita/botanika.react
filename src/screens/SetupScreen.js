// @flow
/**
 * Šiame ekrane vartotojui duodama pasirinkti kalbą
 * ir botanikos sodą, kurio vaizdą jis norėtu matyti aplikacijoje.
 * Pasirinkti elementai (kalba ir sodo id) yra saugojami atmintinėje su AsyncStorage.
 */
import React, { Component } from "react";
import { NavigationScreenProps, TabNavigator } from "react-navigation";
import { connect } from "react-redux";
import { ButtonView, Link, SetupTabView } from "../components";
import { xquery } from "../plugins/XDB";
import * as actions from "../redux/actions";

const setup = {
  language: false,
  garden: false
};

type TabProps = NavigationScreenProps & {
  onSelect: any,
  locale: string,
  gardens: any[],
  garden: number,
  navigation: NavigationScreenProps,
  lang: any,
  onSelect: void,
  onEnd: void
};

const FirstTab = ({ locale, lang, onSelect }: TabProps) => {
  const langOpts = [["lt", "lietuvių"], ["en", "english"], ["ru", "русский"]];

  return (
    <SetupTabView message={lang.pickLanguage}>
      {langOpts.map(opt => (
        <Link
          key={opt[0]}
          title={opt[1].toUpperCase()}
          active={opt[0] === locale}
          onPress={() => onSelect(opt[0])}
        />
      ))}
    </SetupTabView>
  );
};

const SecondTab = ({
  lang,
  navigation,
  garden,
  gardens,
  onSelect,
  onEnd
}: TabProps) => {
  const isDONE = setup.language === true && setup.garden === true;

  return (
    <SetupTabView message={lang.pickGarden} back={() => navigation.goBack()}>
      {gardens.map(g => (
        <Link
          key={g.GardenId}
          title={g.Name}
          style={{ fontSize: 14, maxWidth: 230, marginBottom: 8 }}
          active={g.GardenId === garden}
          onPress={() => onSelect(g.GardenId)}
        />
      ))}
      <ButtonView active={isDONE} onPress={() => onEnd()} title={lang.begin} />
    </SetupTabView>
  );
};

type SetupScreenProps = NavigationScreenProps & {
  language: string
};
type SetupScreenState = {
  language: string,
  gardens: any[],
  garden: any
};
class SetupScreen extends Component<SetupScreenProps, SetupScreenState> {
  constructor(props: SetupScreenProps) {
    super(props);
    this.state = {
      language: props.language,
      garden: null,
      gardens: []
    };
    this.tabNames = ["firstTab", "secondTab"];

    this.setLanguage = this.setLanguage.bind(this);
    this.setGarden = this.setGarden.bind(this);
    this.startSync = this.startSync.bind(this);
  }

  componentDidMount() {
    this.navigator._navigation.navigate(this.tabNames[0]);
    xquery("SELECT GardenId, Name FROM Gardens").then(result => {
      console.log(result);
      this.setState({
        ...this.state,
        gardens: result.data,
        garden: null
      });
    });
  }

  componentWillUnmount() {
    this.navigator._navigation.navigate(this.tabNames[0]);
  }

  componentDidUpdate() {
    let tab = 1;

    if (setup.language === true) {
      tab = 2;
    }

    const tabName = this.tabNames[tab - 1];

    // eslint-disable-next-line
    this.navigator._navigation.navigate(tabName);
  }

  setLanguage(code: string) {
    if (!code) return;

    this.props.setLanguage(code);

    setup.language = true;

    this.setState({
      ...this.state,
      language: code
    });
  }

  setGarden(id: number) {
    if (!id) return;

    this.props.setGarden(id);

    setup.garden = true;
    this.setState({
      ...this.state,
      garden: id
    });
  }

  startSync() {
    this.props.navigation.navigate("Sync");
  }

  tabNames: string[];
  navigator: any;

  render() {
    const Tabs = TabNavigator(
      {
        [this.tabNames[0]]: {
          screen: _ => <FirstTab {...this.props} onSelect={this.setLanguage} />
        },
        [this.tabNames[1]]: {
          screen: props => (
            <SecondTab
              {...props}
              lang={this.props.lang}
              garden={this.props.garden}
              gardens={this.state.gardens}
              onSelect={this.setGarden}
              onEnd={this.startSync}
            />
          )
        }
      },
      {
        animationEnabled: true,
        lazy: false,
        swipeEnabled: true,
        initialRouteName: this.tabNames[0],
        navigationOptions: {
          tabBarVisible: false
        }
      }
    );

    return (
      <Tabs
        ref={nav => {
          this.navigator = nav;
        }}
      />
    );
  }
}

const mapStateToProps = state => ({
  locale: state.language.locale,
  lang: state.language.lang,
  garden: state.garden.garden
});

export default connect(mapStateToProps, actions)(SetupScreen);
