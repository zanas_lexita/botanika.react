import RNFS from "react-native-fs";
import { Platform, StyleSheet } from "react-native";

const DEBUG = false;
const DEBUG_OPTIONS = {
  DOWNLOAD: false,
  START: false
};

const SERVER_ROOT = "https://lbs.lexitacrm.lt/";
const REMOTE_ROOT = `${SERVER_ROOT}app/`;

const APP_KEY = "botanika";
const STORAGE_KEY = `@${APP_KEY}`;

const COLORS = {
  primary: "#4CAF50",
  primary_alt: "#8BC34A",
  secondary: "#FFA000",
  text: {
    primary: "rgba(0, 0, 0, 0.87)",
    secondary: "rgba(0, 0, 0, 0.54)",
    helper: "rgba(0, 0, 0, 0.38)",
    divider: "rgba(0, 0, 0, 0.12)"
  },
  textWhite: {
    primary: "rgba(255,255,255, 1)",
    secondary: "rgba(255,255,255, .7)",
    helper: "rgba(255,255,255, .5)",
    divider: "rgba(255,255,255, 0.12)"
  }
};

const TEXT = StyleSheet.create({
  primary: {
    fontSize: 16
  },
  secondary: {
    fontSize: 14
  },
  helper: {
    fontSize: 12
  },
  heading: {
    fontSize: 24
  }
});

const KEYS = {
  language: `${STORAGE_KEY}:language`,
  garden: `${STORAGE_KEY}:garden`
};

const DIRECTORIES = {
  appDb: "app.db", // Vidinė programėlės duombazę, į kurią siųsime užklausas
  filesToSync: ["upload.zip", "tiles.zip"], // Failai, kuriuos reiktų gauti iš serverio ROOT
  remoteDb: "lbs.db", // Duombazė, kurią siunčiames iš serverio
  remoteRoot: REMOTE_ROOT, // Serverio root
  md5: `${REMOTE_ROOT}md5.php`
};

const ENVIRONMENT = {
  audioDir: `${RNFS.DocumentDirectoryPath}/audio`,
  googleKey: Platform.select({
    ios: "AIzaSyAG4oijQPdfPL7psf7wPhfyuIjDPJN4CoA",
    android: "AIzaSyDxin9KVyjkLB0ptkwZJ44NqhQW6U2TyzM"
  }),
  localMd5: `${RNFS.DocumentDirectoryPath}/md5.json`,
  remoteMd5: DIRECTORIES.md5,
  tilesPath: `${RNFS.DocumentDirectoryPath}/tiles/{z}/{x}/{y}.png`,
  videoDir: `${RNFS.DocumentDirectoryPath}/video`
};

export { COLORS, KEYS, DIRECTORIES, DEBUG, DEBUG_OPTIONS, ENVIRONMENT, TEXT };
