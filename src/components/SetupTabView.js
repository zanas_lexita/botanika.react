import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { BackButton, Line, StartCard } from "../components";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  messageText: {
    fontSize: 14,
    color: "rgba(0, 0, 0, 0.38)",
    textAlign: "center"
  }
});

type ChildHolder = {
  children: any[]
};
type SetupTabViewProps = ChildHolder & {
  back?: any,
  message: string
};
const SetupTabView = ({ back, message, children }: SetupTabViewProps) => {
  return (
    <View style={[styles.container, { backgroundColor: "#fafafa" }]}>
      {back ? <BackButton onPress={() => back()} /> : <View />}
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <StartCard>
          <Text style={styles.messageText}>{message}</Text>
          <Line />
          <View style={{ marginTop: 4, marginBottom: 8 }}>{children}</View>
        </StartCard>
      </View>
    </View>
  );
};

SetupTabView.defaultProps = {
  back: false
};

export default SetupTabView;
