import { createIconSetFromIcoMoon } from "react-native-vector-icons";
import icomoonConfig from "../assets/misc/icomoon/selection.json";

module.exports = createIconSetFromIcoMoon(icomoonConfig);
