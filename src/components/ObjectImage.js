import React from "react";
import { View, Text, StyleSheet, Image, Dimensions } from "react-native";
import LinearGradient from "react-native-linear-gradient";

import { Icon } from "./../components";
import { TEXT, COLORS } from "../globals";

const styles = StyleSheet.create({
  heading: {
    color: "#fff"
  },
  icon: {
    marginRight: 8
  },
  overlay: {
    bottom: 0,
    left: 0,
    paddingBottom: 16,
    paddingHorizontal: 16,
    paddingTop: 32,
    position: "absolute",
    width: "100%"
  },
  sub: {
    color: COLORS.textWhite.secondary
  },
  subLine: {
    alignItems: "center",
    flexDirection: "row",
    marginTop: 4
  }
});
type ObjectImageProps = {
  icon: string,
  src: string,
  sub: string,
  title: string
};

const ObjectImage = (props: ObjectImageProps) => {
  const { icon, src, sub, title, ...other } = props;
  const aspectRatio = 2 / 1;
  const aspectHeight = Dimensions.get("screen").width * 1 / aspectRatio;

  return (
    <View {...other}>
      <Image source={{ uri: src }} style={{ height: aspectHeight }} />
      <LinearGradient
        colors={["rgba(0,0,0,0)", "rgba(0,0,0,1)"]}
        style={styles.overlay}
      >
        <Text style={[styles.heading, TEXT.heading]}>{title}</Text>
        <View style={[styles.subLine]}>
          <Icon
            style={styles.icon}
            color={COLORS.textWhite.secondary}
            name={icon}
            size={18}
          />
          <Text style={{ color: COLORS.textWhite.secondary }}>{sub}</Text>
        </View>
      </LinearGradient>
    </View>
  );
};

export default ObjectImage;
export { ObjectImage };
