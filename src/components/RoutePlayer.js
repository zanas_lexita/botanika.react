import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform
} from "react-native";
import { connect } from "react-redux";
import Icon from "./Icon";
import { COLORS } from "../globals";
import Bar from "./Bar";
import Sound from "react-native-sound";
import { secondsToTime } from "../plugins/Utilities";

type RoutePlayerProps = {
  objects: any[],
  currentObjectIndex: number
};
type RoutePlayerState = {
  currentObjectIndex: boolean,
  isPlaying: boolean,
  duration: number
};
class RoutePlayer extends React.Component<RoutePlayerProps, RoutePlayerState> {
  constructor(props) {
    super(props);

    this.state = {
      isPlaying: props.play
    };
  }

  play() {
    this.sound.play(success => {
      if (success) {
        console.log("Sound finished.");
        this.stop();
      } else {
        console.log("Playback failed due to sound decoding errors.");
        this.stop();
      }
    });

    this.setState({
      ...this.state,
      isPlaying: true,
      duration: 0,
      totalDuration: parseInt(this.sound.getDuration(), 10)
    });

    this.ticker = setInterval(() => {
      this.sound.getCurrentTime(seconds => {
        this.setState({
          ...this.state,
          duration: Math.round(seconds, 10)
        });
      });
    }, 1000);
  }

  toggle(state = !this.state.isPlaying) {
    if (state === true) {
      this.play();
    } else {
      this.stop();
    }
  }

  pause() {
    this.sound.pause();

    clearInterval(this.ticker);
    this.setState({ ...this.state, isPlaying: false });
  }

  stop() {
    this.sound.stop(() => {
      this.sound.reset();
    });

    clearInterval(this.ticker);
    this.setState({ ...this.state, isPlaying: false });
  }

  prepareSound(props = this.props) {
    const { objects, currentObjectIndex, play } = props;
    const currentObj = objects[currentObjectIndex];
    const soundUrl = currentObj && currentObj.MainAudio;
    if (soundUrl) {
      this.sound = new Sound(
        soundUrl,
        Platform.select({ ios: Sound.DOCUMENT, android: Sound.MAIN_BUNDLE }),
        error => {
          if (error) {
            console.log("failed to load the sound", error);
          }
        }
      );

      if (play) {
        setTimeout(() => {
          this.play();
        }, 100);
      }
    }
  }

  /* -------------------------------------------------------- */

  componentDidMount() {
    this.prepareSound();
  }

  componentWillUnmount() {
    if (this.sound) this.sound.release();
  }

  componentWillReceiveProps(nextProps) {
    this.prepareSound(nextProps);
  }

  render() {
    const { objects, currentObjectIndex, play } = this.props;
    const { isPlaying, duration, totalDuration } = this.state;

    const currentObj = objects[currentObjectIndex];

    return (
      <View>
        {isPlaying ? (
          <Row
            style={[
              styles.pad,
              { backgroundColor: COLORS.primary, alignItems: "flex-end" }
            ]}
          >
            <Text style={{ color: COLORS.textWhite.secondary, width: 42 }}>
              {secondsToTime(duration, "m:ss")}
            </Text>
            <View style={{ flex: 1 }}>
              <Text
                numberOfLines={1}
                style={[styles.white, { fontWeight: "bold", marginBottom: 4 }]}
              >
                {currentObj && currentObj.Name}
              </Text>
              <Bar
                value={duration}
                end={totalDuration}
                size={12}
                completeColor="#fff"
                uncompleteColor={COLORS.textWhite.helper}
              />
            </View>
            <TouchableOpacity style={{ marginLeft: 16, marginBottom: -2 }}>
              <Icon name="pause" size={24} color="white" />
            </TouchableOpacity>
          </Row>
        ) : (
          <Row
            centered
            style={[styles.pad, { backgroundColor: COLORS.primary }]}
          >
            <Icon
              name="info_outline"
              color={COLORS.textWhite.helper}
              size={24}
              style={{ marginRight: 16 }}
            />
            <Text style={styles.white}>Prieikite prie norimo objekto</Text>
          </Row>
        )}
        <View
          style={[
            styles.pad,
            {
              backgroundColor: "#fff",
              borderBottomColor: "#e0e0e0",
              borderBottomWidth: 1
            }
          ]}
        >
          {objects.map((o, i) => (
            <RouteItem
              key={o.ObjectId}
              name={o.Name}
              isLast={i === objects.length - 1}
              isFirst={i === 0}
            />
          ))}
        </View>
      </View>
    );
  }
}

const Row = ({ children, centered, justified, style = {}, ...other }) => (
  <View
    style={[
      {
        flexDirection: "row",
        alignItems: centered ? "center" : "stretch",
        justifyContent: justified ? "space-between" : "flex-start"
      },
      style
    ]}
    {...other}
  >
    {children}
  </View>
);

const RouteItem = ({ name, isPlaying, isLast, isFirst }) => {
  let iconName = "circle";
  let iconSize = 4;

  if (isFirst) {
    iconName = "dot-circle-o";
  }
  if (isLast) iconName = "flag";
  if (isPlaying) iconName = "play";

  let color = COLORS.text.secondary;

  if (isFirst || isLast || isPlaying) {
    iconSize = 18;
    color = COLORS.primary;
  }

  return (
    <Row centered style={{ marginBottom: 4 }}>
      <View style={{ width: 18, alignItems: "center", marginRight: 16 }}>
        <Icon name={iconName} color={color} size={iconSize} />
      </View>
      <Text style={{ color }}>{name}</Text>
    </Row>
  );
};

RoutePlayer.defaultProps = {};

const styles = StyleSheet.create({
  pad: {
    paddingVertical: 8,
    paddingHorizontal: 16
  },
  white: {
    color: "#fff"
  }
});

const mapStyleToProps = state => ({
  lang: state.language.lang
});

export default connect(mapStyleToProps)(RoutePlayer);
