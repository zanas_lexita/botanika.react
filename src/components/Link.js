import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import { COLORS } from "../globals";

const styles = StyleSheet.create({
  choiceText: {
    fontSize: 26,
    color: "rgba(0, 0, 0, 0.54)",
    textAlign: "center"
  },
  chosenText: {
    color: COLORS.primary
  }
});

type LinkProps = {
  active: boolean,
  onPress?: any,
  style?: any,
  title: string
};
const Link = (props: LinkProps) => {
  const { style, title, onPress, active, ...other } = props;

  return (
    <TouchableOpacity onPress={onPress} {...other}>
      <Text style={[styles.choiceText, active ? styles.chosenText : {}, style]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};
Link.defaultProps = {
  style: {},
  onPress: () => {}
};

export default Link;
