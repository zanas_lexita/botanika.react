import React from "react";
import { View, Platform, TouchableOpacity } from "react-native";
import { Icon } from "../components";

const BackButton = ({ onPress }) => (
  <View
    style={{
      position: "absolute",
      top: 0,
      left: 0,
      zIndex: 1
    }}
  >
    <TouchableOpacity
      style={Platform.select({
        ios: {
          margin: 32,
          marginTop: 40
        },
        android: {
          margin: 32
        }
      })}
      onPress={onPress}
    >
      <Icon name="arrow_back" size={24} color="#a0a0a0" />
    </TouchableOpacity>
  </View>
);

export default BackButton;
