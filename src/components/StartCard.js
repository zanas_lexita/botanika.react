import React from "react";
import { View, Image, StyleSheet } from "react-native";

const LOGO_HEADER = require("../assets/images/LogoGroup.png");
const CARD_RADIUS = 8;
const styles = StyleSheet.create({
  card: {
    backgroundColor: "#fff",
    borderRadius: CARD_RADIUS,
    shadowColor: "#000",
    elevation: 5,
    shadowOpacity: 0.5,
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: 2
    }
  },
  cardPadding: {
    paddingVertical: 12,
    paddingHorizontal: 16
  }
});

const StartCard = ({ children }) => (
  <View style={styles.card}>
    <Image source={LOGO_HEADER} />
    <View style={[styles.cardPadding, { flex: 0 }]}>{children}</View>
  </View>
);

export default StartCard;
