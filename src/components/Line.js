import React from "react";
import { View, Image } from "react-native";

const DIVIDER = require("../assets/images/Divider.png");

const Line = () => (
  <View
    style={{
      marginVertical: 16,
      flex: 0,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center"
    }}
  >
    <Image source={DIVIDER} />
  </View>
);

export default Line;
