import React from "react";
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Platform,
  Animated
} from "react-native";
import { connect } from "react-redux";
import Icon from "./Icon";
import { COLORS } from "../globals";

class InlineSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: "",
      icon: props.icon,
      isOpened: props.open,
      inputAnim: new Animated.Value(props.open ? 1 : 0)
    };
  }

  toggle() {
    const nextState = !this.state.isOpened;
    const nextText = nextState ? this.state.text : "";

    Animated.timing(
      // Animate over time
      this.state.inputAnim, // The animated value to drive
      {
        toValue: nextState ? 1 : 0,
        duration: 500
      }
    ).start(); // Starts the animation

    this.setState({
      ...this.state,
      isOpened: nextState,
      text: nextText
    });

    this.trigger(nextText);
  }

  trigger(text) {
    const { onSearch } = this.props;
    if (typeof onSearch === "function") this.props.onSearch(text);
  }

  render() {
    const { isOpened, icon } = this.state;
    const { lang } = this.props;

    return (
      <View style={styles.container}>
        <Animated.View
          style={{
            width: this.state.inputAnim,
            opacity: this.state.inputAnim,
            flex: this.state.inputAnim
          }}
        >
          <TextInput
            style={styles.input}
            onChangeText={text => {
              text = text.replace(/\s\s+/g, " ").trim();
              this.setState({ text });
              this.trigger(text);
            }}
            value={this.state.text}
            placeholder={lang.search}
          />
        </Animated.View>
        {icon ? (
          <TouchableOpacity onPress={() => this.toggle()} style={styles.button}>
            <Icon
              color={isOpened ? COLORS.text.primary : COLORS.text.secondary}
              name="search"
              size={24}
            />
          </TouchableOpacity>
        ) : (
          <View />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    paddingVertical: 4,
    paddingHorizontal: 8
  },
  input: {
    height: 40,
    backgroundColor: Platform.select({
      ios: "#e0e0e0",
      android: "transparent"
    }),
    paddingVertical: 4,
    paddingHorizontal: 12,
    borderRadius: 8,
    flex: 1
  },
  icon: {
    color: COLORS.text.helper
  },
  iconActive: {
    color: COLORS.text.primary
  },
  container: {
    flex: 0,
    marginVertical: 4,
    flexDirection: "row",
    alignItems: "center"
  }
});

const mapStateToProps = state => ({
  lang: state.language.lang
});

export default connect(mapStateToProps)(InlineSearch);
