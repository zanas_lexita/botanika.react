import React from 'react';
import { Text, StyleSheet, ButtonProperties, TouchableOpacity, View, Platform } from 'react-native';

import { COLORS } from '../globals';

const styles = StyleSheet.create({
  button: {
    backgroundColor: COLORS.primary,
    borderRadius   : 40,
  },
  text: {
    color     : 'white',
    textAlign : 'center',
    padding   : 8,
    fontWeight: '500',
  },
  buttonDisabled: {
    elevation      : 0,
    backgroundColor: '#dfdfdf',
  },
  textDisabled: {
    color: '#a1a1a1',
  },
});

type RoundedButtonProps = ButtonProperties & {};

const RoundedButton = (props: RoundedButtonProps) => {
  const {
    accessibilityLabel,
    color,
    onPress,
    title,
    hasTVPreferredFocus,
    disabled,
    testID,
    style,
    ...other
  } = props;

  const buttonStyles = [styles.button];
  const textStyles = [styles.text];
  const accessibilityTraits = ['button'];

  if (color) {
    buttonStyles.push({ backgroundColor: color });
  }

  if (disabled) {
    buttonStyles.push(styles.buttonDisabled);
    textStyles.push(styles.textDisabled);
    accessibilityTraits.push('disabled');
  }

  const formattedTitle = Platform.OS === 'android' ? title.toUpperCase() : title;

  return (
    <TouchableOpacity
      accessibilityComponentType="button"
      accessibilityLabel={accessibilityLabel}
      accessibilityTraits={accessibilityTraits}
      hasTVPreferredFocus={hasTVPreferredFocus}
      testID={testID}
      disabled={disabled}
      onPress={onPress}
      {...other}
    >
      <View style={[buttonStyles, style]}>
        <Text style={textStyles} disabled={disabled}>
          {formattedTitle}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

module.exports = RoundedButton;
