import React from "react";

import { Platform } from "react-native";

import Sound from "react-native-sound";
// import { AudioUtils, Sound } from "react-native-audios";

import { connect } from "react-redux";
import XButton from "./XButton";
import { secondsToTime } from "../plugins/Utilities";

type AudioButtonProps = {
  audio: string,
  audioName: string
};

class AudioButton extends React.Component<AudioButtonProps> {
  constructor(props) {
    super(props);

    this.state = {
      totalDuration: 0,
      duration: 0,
      isPlaying: false
    };
  }

  sound: Sound;
  ticker: number;

  componentDidMount() {
    const { audio } = this.props;

    this.sound = new Sound(
      audio,
      Platform.select({ ios: Sound.DOCUMENT, android: Sound.MAIN_BUNDLE }),
      error => {
        if (error) {
          console.log("failed to load the sound", error);
        }
      }
    );
  }

  componentWillUnmount() {
    this.sound.release();
  }

  playSound() {
    this.sound.play(success => {
      if (success) {
        console.log("Sound finished.");
        this.stopSound();
      } else {
        console.log("Playback failed due to sound decoding errors.");
        this.stopSound();
      }
    });

    this.setState({
      ...this.state,
      isPlaying: true,
      duration: 0,
      totalDuration: parseInt(this.sound.getDuration(), 10)
    });

    this.ticker = setInterval(() => {
      this.sound.getCurrentTime(seconds => {
        this.setState({
          ...this.state,
          duration: Math.round(seconds, 10)
        });
      });
    }, 1000);
  }

  toggleSound(state = !this.state.isPlaying) {
    if (state === true) {
      this.playSound();
    } else {
      this.stopSound();
    }
  }

  pauseSound() {
    this.sound.pause();

    clearInterval(this.ticker);
    this.setState({ ...this.state, isPlaying: false });
  }

  stopSound() {
    this.sound.stop(() => {
      clearInterval(this.ticker);
      this.setState({ ...this.state, isPlaying: false });
    });
  }

  render() {
    const { lang, audioName, ...other } = this.props;

    const { duration, totalDuration, isPlaying } = this.state;

    const timeLeft = "-" + secondsToTime(totalDuration - duration, "m:ss");

    return (
      <XButton
        {...other}
        icon={isPlaying ? "stop" : "play_arrow"}
        title={isPlaying ? audioName + " " + timeLeft : lang.listenGuide}
        raised
        onPress={() => this.toggleSound()}
      />
    );
  }
}

const mapStateToProps = state => ({
  lang: state.language.lang
});

export default connect(mapStateToProps)(AudioButton);
