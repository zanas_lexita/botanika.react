import React from "react";
import {
  Text,
  StyleSheet,
  ButtonProperties,
  TouchableHighlight,
  View,
  Platform
} from "react-native";

import { COLORS } from "../globals";
import { Icon } from "./";

const styleRules = {
  button: {
    borderRadius: 2,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    minWidth: 64,
    paddingHorizontal: 8,
    height: 36
  },
  rounded: {
    paddingHorizontal: 12,
    borderRadius: 24
  },
  text: {
    color: COLORS.text.secondary,
    textAlign: "center",
    padding: 8,
    fontWeight: "500"
  },
  buttonDisabled: {
    elevation: 0,
    backgroundColor: "#dfdfdf"
  },
  textDisabled: {
    color: "#a1a1a1"
  }
};
const styles = StyleSheet.create(styleRules);

type XButtonProps = ButtonProperties & {};

const XButton = (props: XButtonProps) => {
  const {
    // Main
    title,
    icon,
    onPress,
    // Secondary
    color,
    // Boolean
    disabled,
    primary,
    rounded,
    raised,
    // Misc
    accessibilityLabel,
    hasTVPreferredFocus,
    testID,
    style,
    // Other
    ...other
  } = props;

  const buttonStyles = [styleRules.button];
  const textStyles = [styleRules.text];
  const accessibilityTraits = ["button"];

  if (raised) {
    buttonStyles.push({
      backgroundColor: COLORS.primary,
      elevation: 1
    });
    textStyles.push({ color: "white" });
  }
  if (primary) {
    textStyles.push({ color: COLORS.primary });
  }

  if (color) {
    buttonStyles.push({ backgroundColor: color });
  }

  if (disabled) {
    buttonStyles.push(styles.buttonDisabled);
    textStyles.push(styles.textDisabled);
    accessibilityTraits.push("disabled");
  }

  if (rounded) {
    buttonStyles.push(styles.rounded);
  }

  const textColor = textStyles.reduce((acc, textRules) => {
    // join all rules from array to `acc` object
    Object.keys(textRules).forEach(rule => {
      acc[rule] = textRules[rule];
    });
    return acc;
  }, {}).color;

  const sharedPreferences = {
    accessibilityComponentType: "button",
    accessibilityLabel,
    accessibilityTraits,
    hasTVPreferredFocus,
    testID,
    disabled,
    onPress,
    underlayColor: "rgba(0, 0, 0, 0.06)",
    hitSlop: {
      top: 4,
      bottom: 4,
      left: 8,
      right: 8
    }
  };

  // raised, flat button

  let finalProps = Object.assign({}, sharedPreferences);
  finalProps = Object.assign({}, finalProps, ...other);
  finalProps.style = buttonStyles;
  finalProps.style.push(style);

  const Content = () => {
    const formattedTitle =
      title && Platform.OS === "android" ? title.toUpperCase() : title || "";
    const IconC = icon ? (
      <Icon name={icon} size={24} color={textColor} />
    ) : (
      <View />
    );
    return (
      <View
        style={{
          justifyContent: "center",
          flexDirection: "row",
          alignItems: "center"
        }}
      >
        {IconC}
        <Text style={textStyles} disabled={disabled}>
          {" "}
          {formattedTitle}
        </Text>
      </View>
    );
  };

  return (
    <TouchableHighlight {...finalProps}>
      <Content />
    </TouchableHighlight>
  );
};

module.exports = XButton;
