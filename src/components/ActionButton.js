import React from 'react';
import { StyleSheet, TouchableOpacity, ColorPropType } from 'react-native';

import Icon from './../components/Icon';
import { COLORS } from './../globals';

const styles = StyleSheet.create({
  button: {
    alignItems     : 'center',
    backgroundColor: COLORS.primary,
    borderRadius   : 56,
    bottom         : 0,
    elevation      : 3,
    flex           : 0,
    height         : 56,
    justifyContent : 'center',
    margin         : 24,
    position       : 'absolute',
    right          : 0,
    shadowColor    : '#000',
    shadowOffset   : { width: 0, height: 3 },
    shadowOpacity  : 0.3,
    shadowRadius   : 4,
    width          : 56,
    zIndex         : 1000,
  },
});

type ActionButtonProps = {
  icon: string,
  style: any,
  iconColor: ColorPropType,
};

const ActionButton = (props: ActionButtonProps) => {
  const {
    icon, style, iconColor, ...others
  } = props;

  return (
    <TouchableOpacity style={[styles.button, style]} {...others}>
      <Icon name={icon} size={24} color={iconColor || 'white'} />
    </TouchableOpacity>
  );
};

export default ActionButton;
export { ActionButton };
