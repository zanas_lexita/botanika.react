import React from "react";
import { View, Text, StyleSheet, ActivityIndicator } from "react-native";
import { COLORS } from "../globals";
import Bar, { BarProps } from "./Bar";

const styles = StyleSheet.create({
  bar: {
    backgroundColor: "#f0f0f0",
    height: 2,
    borderRadius: 2,
    flexDirection: "row"
  },
  progress: {
    backgroundColor: COLORS.primary
  },
  title: {
    color: "#707070",
    textAlign: "center",
    marginBottom: 8
  }
});

type ProgressBarProps = BarProps & {
  title?: string, // Tekstas, rodomas šalia progreso bloko
  style?: any
};

const progress = (start = 0, end = 100, value = 0) => {
  if (value > end) return 1;
  if (value === 0 && end === 0) return 0;

  return (value - start) / (end - start);
};

const ProgressBar = (props: ProgressBarProps) => {
  const { style, start, end, value, title } = props;

  return (
    <View style={style}>
      <ActivityIndicator
        style={{ marginBottom: 32 }}
        color="rgba(0, 0, 0, .2)"
      />
      <Text style={[styles.title, !title && { display: "none" }]}>
        {title.toString()}
      </Text>
      <Bar start={start} end={end} value={value} />
    </View>
  );
};

export { ProgressBar, progress };
