import React from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import MapView, {
  Callout,
  LocalTile,
  MapViewProps,
  UrlTile,
  Marker,
  Polyline
} from "react-native-maps";
import { connect } from "react-redux";
import { ENVIRONMENT } from "../globals";
import { GardenObject } from "../plugins/Types";
import XButton from "./XButton";
import route from "./route.json";

const styles = StyleSheet.create({
  fullscreen: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  }
});

type XMapProps = MapViewProps & {
  places: GardenObject[]
};

type XMapState = XMapProps & {};

class XMap extends React.Component<XMapProps, XMapState> {
  watchId: number;

  locate() {
    const { map } = this;
    const { location } = this.props;

    if (!map || !location) return;

    map.animateToRegion({
      latitude: location.lat,
      longitude: location.lng,
      latitudeDelta: 0.002,
      longitudeDelta: 0.002
    });
  }

  componentDidMount() {
    if (typeof this.props.onRef === "function") this.props.onRef(this);
  }

  componentWillUnmount() {
    if (typeof this.props.onRef === "function") this.props.onRef(undefined);
  }

  render() {
    const { places, region, lang, navStack, ...otherProps } = this.props;

    // Tooltip content
    const TooltipView = ({ title, text, buttonText, onPress }) => (
      <View style={{ width: 200 }}>
        <Text style={{ fontSize: 16, fontWeight: "600" }}>{title}</Text>
        <Text
          style={{ marginTop: 2, marginBottom: 4 }}
          numberOfLines={3}
          ellipsizeMode="tail"
        >
          {text}
        </Text>
        <XButton title={buttonText} onPress={onPress} primary />
      </View>
    );

    return (
      <View style={{ flex: 1 }}>
        <MapView
          ref={ref => (this.map = ref)}
          style={styles.fullscreen}
          mapType={Platform.OS === "android" ? "none" : "standard"}
          rotateEnabled={false}
          region={region}
          initialRegion={region}
          showsUserLocation
          // {...otherProps}
        >
          {/* URL TILES FOR DEV */}
          <UrlTile urlTemplate="https://c.tile.openstreetmap.org/{z}/{x}/{y}.png" />
          {/* TILES */}
          <LocalTile pathTemplate={ENVIRONMENT.tilesPath} tileSize={256} />

          {/* MARKERS */}
          {places.map(place => (
            <Marker
              key={place.ObjectId}
              coordinate={{
                latitude: Number(place.Latitude),
                longitude: Number(place.Longitude)
              }}
            >
              <Callout>
                <TooltipView
                  title={place.Name}
                  text={place.Description}
                  buttonText={lang.readMore + "..."}
                  onPress={() => {
                    navStack.navigate("Object", place);
                  }}
                />
              </Callout>
            </Marker>
          ))}

          {/* POLYLINES (ROUTES) */}
          {
            <Polyline
              coordinates={route}
              zIndex={100}
              strokeColor="blue"
              strokeWidth={4}
            />
          }
        </MapView>
      </View>
    );
  }
}

XMap.defaultProps = {
  places: []
};

const mapStateToProps = state => ({
  location: state.location.location,
  lang: state.language.lang
});

export default connect(mapStateToProps)(XMap);
