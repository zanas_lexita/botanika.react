import React from "react";
import { Platform, StyleSheet, View, Text, Dimensions } from "react-native";
import MapView, {
  LocalTile,
  MapViewProps,
  Marker,
  UrlTile,
  Polyline,
  Callout
} from "react-native-maps";
import { connect } from "react-redux";
import { ENVIRONMENT, COLORS } from "../globals";
import { GardenObject } from "../plugins/Types";
import XButton from "./XButton";
import RoutePlayer from "./RoutePlayer";

const styles = StyleSheet.create({
  fullscreen: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  }
});

type XMapRouteProps = MapViewProps & {
  route: GardenObject[]
};

type XMapRouteState = XMapRouteProps & {};

class XMapRoute extends React.Component<XMapRouteProps, XMapRouteState> {
  watchId: number;

  locate() {
    const { map } = this;
    const { location } = this.props;

    if (!map || !location) return;

    map.animateToRegion({
      latitude: location.lat,
      longitude: location.lng,
      latitudeDelta: 0.002,
      longitudeDelta: 0.002
    });
  }

  componentDidMount() {
    if (typeof this.props.onRef === "function") this.props.onRef(this);
  }

  componentWillUnmount() {
    if (typeof this.props.onRef === "function") this.props.onRef(undefined);
  }

  render() {
    const { route, region, lang, ...otherProps } = this.props;

    const mapAspectRatio = 5 / 4;
    const mapAspectHeight = Dimensions.get("screen").width * 1 / mapAspectRatio;

    // Tooltip content
    const TooltipView = ({ title, text, buttonText, onPress }) => (
      <View style={{ width: 200 }}>
        <Text style={{ fontSize: 16, fontWeight: "600" }}>{title}</Text>
        <Text
          style={{ marginTop: 2, marginBottom: 4 }}
          numberOfLines={3}
          ellipsizeMode="tail"
        >
          {text}
        </Text>
        <XButton title={buttonText} onPress={onPress} primary />
      </View>
    );

    return (
      <View style={{ flex: 0 }}>
        <MapView
          ref={ref => (this.map = ref)}
          style={{ height: mapAspectHeight, width: "100%" }}
          mapType={Platform.OS === "android" ? "none" : "standard"}
          rotateEnabled={false}
          region={region}
          initialRegion={region}
          showsUserLocation
        >
          {/* URL TILES FOR DEV */}
          <UrlTile urlTemplate="https://c.tile.openstreetmap.org/{z}/{x}/{y}.png" />
          {/* TILES */}
          <LocalTile pathTemplate={ENVIRONMENT.tilesPath} tileSize={256} />

          {/* MARKERS */}
          {route.map(place => (
            <Marker
              key={place.ObjectId}
              coordinate={{
                latitude: Number(place.Latitude),
                longitude: Number(place.Longitude)
              }}
            >
              <Callout>
                <TooltipView
                  title={place.Name}
                  text={place.Description}
                  buttonText={lang.readMore + "..."}
                  onPress={() => {
                    NavigationStackService.navigate("Object", place);
                  }}
                />
              </Callout>
            </Marker>
          ))}

          {/* POLYLINES (ROUTES) */}
          <Polyline
            coordinates={route.map(r => ({
              latitude: r.Latitude,
              longitude: r.Longitude
            }))}
            zIndex={100}
            strokeColor="blue"
            strokeWidth={4}
          />
        </MapView>
        {/* ROUTE OBJECT LIST */}
        <RoutePlayer objects={route} currentObjectIndex={0} play />
      </View>
    );
  }
}

XMapRoute.defaultProps = {
  route: []
};

const mapStateToProps = state => ({
  location: state.location.location,
  lang: state.language.lang
});

export default connect(mapStateToProps)(XMapRoute);
