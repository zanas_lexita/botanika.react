import React from "react";
import { View, StyleSheet } from "react-native";
import { COLORS } from "../globals";

const styles = StyleSheet.create({
  bar: {
    backgroundColor: "#f0f0f0",
    height: 2,
    borderRadius: 2,
    flexDirection: "row"
  },
  progress: {
    backgroundColor: COLORS.primary
  }
});

export type BarProps = {
  end?: number, // Progreso pabaigos atskaitos taškas
  start?: number, // Progreso pradžios atskaitos taškas
  value: number, // Progreso reikšmė tarp pradžios ir pabaigos taškų,
  uncompleteColor?: string,
  completeColor?: string,
  size?: string
};

const progress = (start = 0, end = 100, value = 0) => {
  if (value > end) return 1;
  if (value === 0 && end === 0) return 0;

  return (value - start) / (end - start);
};

const Bar = (props: BarProps) => {
  const {
    style,
    start,
    end,
    value,
    size = 2,
    uncompleteColor = "#f0f0f0",
    completeColor = COLORS.primary,
    ...other
  } = props;

  const progressPercentage = progress(start, end, value);

  return (
    <View style={style}>
      <View
        style={[
          styles.bar,
          {
            backgroundColor: uncompleteColor,
            height: size
          }
        ]}
        {...other}
      >
        <View
          style={[
            styles.bar,
            styles.progress,
            {
              flex: progressPercentage,
              backgroundColor: completeColor,
              height: size
            }
          ]}
        />
        <View style={{ flex: 1 - progressPercentage }} />
      </View>
    </View>
  );
};

Bar.defaultProps = {
  end: 100,
  start: 0,
  title: "",
  style: {}
};

export default Bar;
