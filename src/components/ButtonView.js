import React from "react";
import { View } from "react-native";
import { RoundedButton, Line } from "./";

type ButtonViewProps = {
  active: boolean,
  onPress: any
};
const ButtonView = (props: ButtonViewProps) => {
  const { active, onPress, ...other } = props;

  const activeStyle = [
    { alignSelf: "stretch" },
    active ? {} : { display: "none" }
  ];

  return (
    <View style={activeStyle}>
      <Line />
      <RoundedButton {...other} onPress={onPress} />
    </View>
  );
};

export default ButtonView;
