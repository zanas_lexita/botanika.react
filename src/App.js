import React from "react";
import { StatusBar, StyleSheet, View } from "react-native";
import { Provider } from "react-redux";
import { Navigator } from "./navigators";
import Store from "./redux/Store";

export default () => (
  <Provider store={Store}>
    <View style={styles.container}>
      <StatusBar hidden />
      <Navigator style={styles.container} />
    </View>
  </Provider>
);

const styles = StyleSheet.create({
  container: { flex: 1 }
});
