import React from "react";
import { StackNavigator } from "react-navigation";
import { ObjectListScreen, ObjectScreen, RouteScreen } from "../screens";
import AppTabNavigator from "./AppTabNavigator";

const AppStack = StackNavigator({
  Tabs: {
    screen: AppTabNavigator,
    navigationOptions: {
      header: null
    }
  },
  List: {
    screen: ObjectListScreen,
    navigationOptions: {
      title: "Katalogas",
      header: null
    }
  },
  Object: {
    screen: ObjectScreen,
    navigationOptions: {
      header: null
    }
  },
  Route: {
    screen: RouteScreen,
    navigationOptions: {
      header: null
    }
  }
});

export default ({ navigation }) => (
  <AppStack screenProps={{ navSwitch: navigation }} />
);
