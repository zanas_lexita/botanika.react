/* eslint-disable no-underscore-dangle */
import { NavigationActions } from "react-navigation";

let _navigator;
let _navigatorStackRef;

const setTopLevelNavigator = ref => {
  _navigator = ref;
};
const setStackLevelNavigator = ref => {
  _navigatorStackRef = ref;
};

const logNavigation = (routeName, params, ref) => {
  console.log(
    "%cNAVIGATING",
    "background-color: #6b52ae; color: #fff; padding: 0 6px; border-radius: 2px;",
    routeName,
    params,
    ref
  );
};

const navigate = (routeName, params) => {
  logNavigation(routeName, params, _navigator);
  _navigator.dispatch(
    NavigationActions.navigate({
      type: NavigationActions.NAVIGATE,
      routeName,
      params
    })
  );
};

const navigateStack = (routeName, params) => {
  logNavigation(routeName, params, _navigatorStackRef);
  _navigatorStackRef.dispatch(
    NavigationActions.navigate({
      type: NavigationActions.NAVIGATE,
      routeName,
      params
    })
  );
};

// add other navigation functions that you need and export them

export default {
  navigate,
  navigateStack,
  setTopLevelNavigator,
  setStackLevelNavigator
};
