import React from "react";
import { SwitchNavigator } from "react-navigation";
import { connect } from "react-redux";
import * as actions from "../redux/actions";
import {
  LoadingScreen,
  NoNetworkScreen,
  SetupScreen,
  SyncScreen
} from "../screens";
import AppStackNavigator from "./AppStackNavigator";
import NavigationService from "./NavigationService";

const AppNavigator = SwitchNavigator(
  {
    Loading: LoadingScreen,
    App: AppStackNavigator,
    Setup: SetupScreen,
    Sync: SyncScreen,
    NoNetwork: NoNetworkScreen
  },
  {
    initialRouteName: "Loading"
  }
);

class Navigator extends React.Component {
  componentDidMount() {
    // Initialize Redux default state
    this.props.setLanguage();
    this.props.setGarden();
    this.props.watchLocation();
  }
  render() {
    return (
      <AppNavigator
        ref={navigationRef => {
          NavigationService.setTopLevelNavigator(navigationRef);
        }}
      />
    );
  }
}

export default connect(() => ({}), actions)(Navigator);
