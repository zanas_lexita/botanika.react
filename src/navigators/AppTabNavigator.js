import React from "react";
import { View } from "react-native";
import {
  TabBarBottom,
  TabNavigator,
  TabNavigatorConfig
} from "react-navigation";
import { connect } from "react-redux";
import { Icon } from "../components";
import { COLORS } from "../globals";
import * as actions from "../redux/actions";
import { ARTab, MapTab, RouteTab } from "../screens/tabs";
import { xlog } from "../plugins/Utilities";

class AppTabNavigator extends React.Component {
  render() {
    const { lang, objects, gardenInfo, screenProps } = this.props;

    const TabRoutes = {
      Route: { screen: RouteTab, navigationOptions: { title: lang.routes } },
      Map: { screen: MapTab, navigationOptions: { title: lang.map } },
      AR: { screen: ARTab, navigationOptions: { title: lang.arCamera } }
    };

    const TabScreenConfig: TabNavigatorConfig = {
      tabBarPosition: "bottom",
      initialRouteName: "Route",
      animationEnabled: true,
      swipeEnabled: false,
      tabBarComponent: TabBarBottom,
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: iconProps => {
          const { tintColor } = iconProps;
          const { routeName } = navigation.state;
          let iconName;

          if (routeName === "Map") {
            iconName = "map";
          } else if (routeName === "AR") {
            iconName = "camera";
          } else if (routeName === "Route") {
            iconName = "routes";
          }

          return <Icon name={iconName} size={24} color={tintColor} />;
        }
      }),
      tabBarOptions: {
        activeTintColor: COLORS.primary,
        inactiveTintColor: COLORS.text.secondary,
        showIcon: true,
        showLabel: true,
        style: {
          backgroundColor: "white"
        }
      }
    };

    const Tabs = TabNavigator(TabRoutes, TabScreenConfig);

    return (
      <Tabs
        screenProps={{
          objects,
          gardenInfo,
          navStack: this.props.navigation,
          navSwitch: screenProps.navSwitch
        }}
      />
    );
  }
}

const mapStateToProps = state => ({
  lang: state.language.lang,
  garden: state.garden.garden,
  objects: state.fetch.objects,
  gardenInfo: state.fetch.gardenInfo
});

export default connect(mapStateToProps, actions)(AppTabNavigator);
