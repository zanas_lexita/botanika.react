import { DIRECTORIES, DEBUG, DEBUG_OPTIONS } from "../globals";
import RNFS from "react-native-fs";
import RNFetchBlob from "react-native-fetch-blob";
import { MD5 } from "./../plugins/Types";
import { unzip } from "react-native-zip-archive";

export const unzipFiles = (filesList: string[]) => {
  if (filesList.length === 0) {
    console.log("ALL FILES ALREADY UNZIPPED. Skipping...");
    return Promise.resolve();
  }

  const unzippings = filesList.reduce((list, fileName) => {
    const zipRg = /\.zip\b/g;

    if (zipRg.test(fileName)) {
      const dir = RNFetchBlob.fs.dirs.DocumentDir;
      const sourcePath = `${dir}/${fileName}`;
      // const targetPath = sourcePath.replace(zipRg, '/'); // tiles.zip -> tiles/
      // const targetPath = sourcePath; // zips already have folders inside
      const targetPath = dir; // zips already have folders inside
      list.push(
        unzip(sourcePath, targetPath)
          .then(path => {
            console.log(`UNZIP | Done: ${path}`);
            return sourcePath;
          })
          // After unzip, delete original zips
          .then(source => RNFS.unlink(source))
      );
    }
    return list;
  }, []);
  return Promise.all(unzippings);
};
export const getTotalSize = (md5JSON: MD5): number =>
  md5JSON.reduce((sum, entry) => sum + entry.size, 0);

export const downloadFiles = (files, onProgress) => {
  if (files.length === 0) {
    console.log("ALL FILES ALREADY DOWNLOADED. Skipping...");
    return Promise.resolve(files);
  }

  console.log(`DOWNLOADING ${files.length} FILES`, files);

  const progress = files.reduce((acc, name) => {
    acc[name] = 0;
    return acc;
  }, {});

  const downloads = files.map((fileName: string) => {
    const uri = DIRECTORIES.remoteRoot + fileName;

    return RNFetchBlob.config({
      path: `${RNFetchBlob.fs.dirs.DocumentDir}/${fileName}`,
      overwrite: true
    })
      .fetch("GET", uri)
      .progress({ interval: 500 }, received => {
        progress[fileName] = received;
        const sum = Object.values(progress).reduce((s, p) => s + Number(p), 0);

        if (typeof onProgress === "function") {
          onProgress(sum);
        }
      })
      .then(res => {
        console.log(`FETCH | Downloaded: ${res.path()}`, res);
      });
  });

  return Promise.all(downloads)
    .then(() => files)
    .catch(err => console.log("DOWNLOAD ERROR:", err));
};

/** Patikrina, ar pasiųsti aplikacijos failai yra naujausi
 * ! Prielaidos
 * ! - Jeigu egzistuoja md5 failas, jame nurodyti failai yra parsiųsti
 *
 * Galimos situacijos
 * # 1 Offline, not synced -> Neįmanoma naudotis aplikacija.
 *     "Aplikacija nėra pilnai įdiegta. Reikalingas interneto ryšys"
 * # 2 Offline, "synced" -> Rodome ką turime.
 * # 3 Online, not synced -> Parsiunčiame nesutampančius failus
 * # 4 Online, synced -> visi failai sutampa su serveryje esančiais
 *
 * Workflow
 * 1. Parsiunčiame MD5 sąrašą iš serverio
 * 2. Palyginame MD5 kodus. Failų pavadinimus saugojame, jeigu md5 hašai nesutampa
 * 3. Siunčiame reikiamus failus
 */
export const syncFiles = (
  md5Url: string = DIRECTORIES.md5,
  onProgress?: void,
  filesToSync: string[],
  saveChanges: boolean = false
) => {
  console.log(`FETCHING | ${md5Url}`);

  // Saugome get parametra
  const rg = /.*?=(\d+)/;
  const match = md5Url.toString().match(rg);
  let modifier = (match && match[1]) || "";
  if (modifier !== "") modifier = "_" + modifier;

  const localMD5Path = `${RNFS.DocumentDirectoryPath}/md5${modifier}.json`;

  return new Promise((resolve, reject) => {
    let totalSize;
    let downloaded;

    const updateTotalSize = (md5: MD5) => {
      totalSize = getTotalSize(md5);
      console.log("TOTAL DOWNLOAD SIZE: ", totalSize);
    };

    const updateDownloadedSize = size => {
      downloaded = size;
      if (typeof onProgress === "function") {
        onProgress(downloaded, totalSize);
      }
    };

    fetch(md5Url, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      // Sync MD5 Files
      .then(
        response => response.json(),
        err => console.log("FETCH ERROR | ", err)
      )
      .then((remoteMd5: MD5[]) => {
        console.log("FETCHED MD5 DATA | Checking local files... ", remoteMd5);
        let didExist = false;
        return (
          RNFS.exists(localMD5Path)
            // Get local md5 file, if doesn't exist, create one. Promise<string>
            .then((exists: boolean): Promise<MD5[]> => {
              if (exists === true) {
                didExist = true;
                return RNFS.readFile(localMD5Path).then(fileContents =>
                  JSON.parse(fileContents)
                );
              }
              return Promise.resolve(remoteMd5);
            })
            // Get files that are not matching MD5s
            .then(localMd5 => {
              localMd5.sort((a, b) => Number(a.file > b.file));
              remoteMd5.sort((a, b) => Number(a.file > b.file));

              // DEBUG -- visada siųs failus
              if (DEBUG === true && DEBUG_OPTIONS.DOWNLOAD === true) {
                didExist = false;
              }

              if (didExist) {
                // Filter only missing files
                const missing: MD5[] = remoteMd5.reduce(
                  (fileList, rEntry, i) => {
                    const lEntry = localMd5[i];
                    if (lEntry && lEntry.md5 !== rEntry.md5) {
                      fileList.push(rEntry);
                    } else if (lEntry === undefined) {
                      fileList.push(rEntry);
                    }
                    return fileList;
                  },
                  []
                );

                return missing;
              }

              // Download every file.
              return remoteMd5;
            })
            // Filter MD5 (if specified)
            .then(md5 => {
              if (filesToSync) {
                return filesToSync.map(
                  fileName => md5.filter(entry => entry.file === fileName)[0]
                );
              }
              return md5;
            })
            // Update total filesize. Map MD5 manifest to only filename array
            .then(md5 => {
              updateTotalSize(md5);
              return md5.map(entry => entry.file);
            })
            // got string[] with filenames to download... ['tiles.zip', 'audio.zip', ...]
            // Download all needed files, updating progress on callback
            .then(files =>
              downloadFiles(files, sum => updateDownloadedSize(sum))
            )
            // Downloaded needed files...
            // Unzip donwloaded files
            .then(filesList => unzipFiles(filesList))
            // Write remote manifest to storage (fully synced)
            .then(() => {
              if (saveChanges) {
                console.log("SYNC | ALL DONE | Updating manifest...");
                return RNFS.writeFile(
                  localMD5Path,
                  JSON.stringify(remoteMd5),
                  "utf8"
                ).then(() => RNFS.readFile(localMD5Path));
              }
            })
        );
      })
      .then(() => {
        resolve();
      })
      .catch((err, ...args) => reject(err, ...args));
  });
};
