// @flow

import { LatLng } from "./Types";

/**
 * Pakeičia dydį paduotą baitais į KB, MB, GB ...
 * @param {number|string} size - failo dydis baitais
 * @param {boolean} si - failo dydžio bazė. Jeigu `true` (default), naudojama 1000, kitu atveju 1024
 * @return {string}
 */
export const humanFilesize = (size: number | string, si: boolean = true) => {
  let s = parseInt(size, 10);

  const thresh = si ? 1000 : 1024;
  if (Math.abs(s) < thresh) {
    return `${s} B`;
  }
  const units = si
    ? ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
    : ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
  let u = -1;
  do {
    s /= thresh;
    ++u;
  } while (Math.abs(s) >= thresh && u < units.length - 1);
  return `${s.toFixed(1)} ${units[u]}`;
};

export const EARTH_RADIUS_IN_METERS = 6371e3;

export const toDegrees = (radians: number) => radians * 180 / Math.PI;
export const toRadians = (degrees: number) => degrees * Math.PI / 180;

/**
 * Paskaičiuoja atstumą metrais tarp dviejų koordinačių (c2 - c1), kurios paduotos objekte.
 * Formatas: LatLng = {lat: number, }
 * @param {LatLng} coord1
 * @param {LatLng} coord2
 * @return {number} atstumas metrais
 */
export const coordDistance = (coord1: LatLng, coord2: LatLng): number => {
  if (!coord1 || !coord2) return 0;

  const R = EARTH_RADIUS_IN_METERS;
  const φ1 = toRadians(coord1.lat);
  const φ2 = toRadians(coord2.lat);
  const Δφ = toRadians(coord2.lat - coord1.lat);
  const Δλ = toRadians(coord2.lng - coord2.lng);

  const a =
    Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
    Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  const d = R * c;

  return d;
};

/**
 * Suformatuoją metrus į human readable formatą.
 * @param {number} meters
 */
export const humanDistance = (meters: number): string => {
  if (!meters) return "";
  const km = Math.floor(meters / 1000);

  if (km) {
    const p = 100;
    return Math.round(meters * p / 1000) / p + " km";
  }

  return Math.round(meters) + " m";
};

/**
 * Paskaičiuoja ėjimo trukmę duotiems metrams įveikti
 * @param {number} meters
 * @param {number} minutesPerKm
 * @return {number} seconds
 */
export const distanceDuration = (
  meters: number,
  minutesPerKm: number = 12.5
): number => {
  if (!meters) return 0;
  return Math.round(meters) / 1000 * minutesPerKm * 60;
};

export const toAscii = (str: string) => {
  const map = {
    ą: "a",
    č: "c",
    ę: "e",
    ė: "e",
    į: "i",
    š: "s",
    ų: "u",
    ū: "u",
    ž: "z",
    Ą: "A",
    Č: "C",
    Ę: "E",
    Ė: "E",
    Į: "I",
    Š: "S",
    Ų: "U",
    Ū: "U",
    Ž: "Z"
  };

  return str.replace(/[^a-zA-Z]/g, l => map[l] || l);
};

export const secondsToTime = (secs, format) => {
  let hr = Math.floor(secs / 3600);
  let min = Math.floor((secs - hr * 3600) / 60);
  let sec = Math.floor(secs - hr * 3600 - min * 60);

  if (hr < 10) hr = "0" + hr;
  if (min < 10) min = "0" + min;
  if (sec < 10) sec = "0" + sec;

  if (format) {
    let formatted = format.replace("hh", hr);
    formatted = formatted.replace("h", hr * 1 + "");
    formatted = formatted.replace("mm", min);
    formatted = formatted.replace("m", min * 1 + "");
    formatted = formatted.replace("ss", sec);
    formatted = formatted.replace("s", sec * 1 + "");
    return formatted;
  }

  return hr + ":" + min + ":" + sec;
};

export const searchify = (str: string) =>
  toAscii(str)
    .toLowerCase()
    .replace(/[.,]/g, "")
    .replace(/([-_]|\s\s+)/g, " ")
    .trim();

// Grąžina nurodytos apskirties ribas MapView komponentui.
export const calculateBounds = (
  northLat: number,
  southLat: number,
  westLng: number,
  eastLng: number
) => {
  if (!northLat || !southLat || !westLng || !eastLng) return;

  northLat = Number(northLat);
  southLat = Number(southLat);
  westLng = Number(westLng);
  eastLng = Number(eastLng);

  const earthRadiusInKM = EARTH_RADIUS_IN_METERS / 1000;

  // Apibrėžtos zonos kraštinių centrų koordinatės
  const center = {
    lat: (northLat + southLat) / 2,
    lng: (eastLng + westLng) / 2
  };
  const borders = {
    top: { lat: northLat, lng: center.lng },
    bottom: { lat: southLat, lng: center.lng },
    left: { lat: center.lat, lng: westLng },
    right: { lat: center.lat, lng: eastLng }
  };

  // Get bigger radius
  const radiusInKM =
    Math.max(
      coordDistance(borders.top, borders.bottom),
      coordDistance(borders.left, borders.right)
    ) /
    1000 / // KM
    2; // Radius

  const radiusInRad = radiusInKM / earthRadiusInKM;

  const longitudeDelta = toDegrees(
    radiusInRad / Math.cos(toRadians(center.lat))
  );
  const latitudeDelta = longitudeDelta;

  return {
    latitude: center.lat,
    longitude: center.lng,
    latitudeDelta: latitudeDelta * 2,
    longitudeDelta: longitudeDelta * 2
  };
};

export const getCurrentPosition = (): Promise<Position> =>
  new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      result => resolve(result),
      err => reject(err),
      {
        timeout: 5000,
        maximumAge: Infinity,
        enableHighAccuracy: true
      }
    );
  });

export const xlog = (msg: string = "XLOG", ...args: any) => {
  console.log(`~~~~~~~~~~~~~~ - ${msg} - ~~~~~~~~~~~~~~`, ...args);
};
