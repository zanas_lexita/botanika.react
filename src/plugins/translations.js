import { ORDER_BY_LOCATION, ORDER_BY_NAME } from "../redux/actions/types";

const TRANSLATIONS = {
  orderBy: {
    en: "Sort by",
    lt: "Rūšiuojama pagal",
    ru: "Сортировать по"
  },
  [ORDER_BY_NAME]: {
    en: "name",
    lt: "pavadinimą",
    ru: "названию"
  },
  [ORDER_BY_LOCATION]: {
    en: "distance",
    lt: "atstumą",
    ru: "расстоянию"
  },
  arCamera: {
    en: "Camera",
    lt: "Kamera",
    ru: "Камера"
  },
  begin: {
    en: "Begin",
    lt: "Pradėti",
    ru: "Начать"
  },
  readMore: {
    en: "Read more",
    lt: "Skaityti toliau",
    ru: "Читать дальше"
  },
  goBack: {
    en: "Go back",
    lt: "Grįžti",
    ru: "Вернуться"
  },
  tryAgain: {
    en: "Restart",
    lt: "Bandyti dar kartą",
    ru: "Попробуйте еще раз"
  },
  noNetwork: {
    en: "Could not connect to the internet",
    lt: "Nepavyko prisijungti prie interneto",
    ru: "Не удается подключиться к Интернету"
  },
  listenGuide: {
    en: "Listen to guide",
    lt: "Klausyti gido",
    ru: "Слушайте гид"
  },
  changeGarden: {
    en: "Change garden",
    lt: "Keisti sodą",
    ru: "Изменить сад"
  },
  loading: {
    en: "Loading",
    lt: "Kraunasi",
    ru: "Загрузка"
  },
  map: {
    en: "Map",
    lt: "Žemėlapis",
    ru: "Карта"
  },
  pickGarden: {
    en: "Choose a garden",
    lt: "Pasirinkite sodą",
    ru: "Выберите сад"
  },
  search: {
    en: "Search",
    lt: "Paieška",
    ru: "Поиск"
  },
  pickLanguage: {
    en: "Choose a language",
    lt: "Pasirinkite kalbą",
    ru: "Выберите язык"
  },
  routes: {
    en: "Routes",
    lt: "Maršrutai",
    ru: "Маршруты"
  },
  objectCount: {
    en: "Object count",
    lt: "Objektų kiekis",
    ru: "Количество позиций"
  }
};

const DICTIONARY = (language = "lt") =>
  Object.keys(TRANSLATIONS).reduce((acc, key) => {
    acc[key] = TRANSLATIONS[key][language];
    return acc;
  }, {});

export { TRANSLATIONS, DICTIONARY };
