import { Platform } from "react-native";
// Failų siuntimui iš serverio
import RNFetchBlob from "react-native-fetch-blob";
// Integracija su lokaliuoju SQLite
import SQLite, { SQLiteDatabase, SQLError } from "react-native-sqlite-storage";
import RNFS from "react-native-fs";

// Projekto konstantos
import { DIRECTORIES } from "../globals";
import { DbResult } from "../plugins/Types";

type SyncParameters = {
  // duombazės talpykla
  REMOTE_ROOT: string,
  // duombazės failo pavadinimas, kurią reikia parsisiųsti
  REMOTE_DB: string,
  // vietinės (local) duombazės pavadinimas, naudojamas aplikacijoje
  LOCAL_DB: string,
  // vietinės (local) duombazės direktorijos adresas
  LOCAL_DB_URI: string
};
class XDB {
  db: SQLiteDatabase;
  SYNC: SyncParameters;
  constructor() {
    this.SYNC = {
      REMOTE_ROOT: DIRECTORIES.remoteRoot,
      REMOTE_DB: DIRECTORIES.remoteDb,
      LOCAL_DB: DIRECTORIES.appDb,
      LOCAL_DB_URI: `${RNFS.DocumentDirectoryPath}/../databases/${
        DIRECTORIES.appDb
      }`
    };
  }

  /** Inicializuoja duombazę aplikacijai pagal (pre-filled) SQLite duombazės
   * @param {boolean} useLocal - Jeigu `true` inicializuojama vidinė duombazė (offline duomenys),
   * kitu atveju offline db ištrinama ir sukuriama naujoji iš pre-filled duomenų.
   * @return {Promise}
   */
  init(useLocal): Promise<SQLiteDatabase> {
    return new Promise((resolve, reject) => {
      // Funkcija duombazei sukurti, kviečiama TIK kai nėra egzistuojančios tuo pačiu vardu
      const { LOCAL_DB } = this.SYNC;

      const openDatabase = () => {
        if (useLocal === true) {
          console.log("xDB | Using local database");
        } else {
          console.log("xDB | Using remote database");
        }

        const dbConfig = {
          name: LOCAL_DB,
          location: "Documents"
          // createFromLocation: Platform.select({
          //   ios: "" + LOCAL_DB,
          //   android: "~" + LOCAL_DB
          // })
        };

        SQLite.openDatabase(
          dbConfig,
          db => {
            console.log("xDB | Database OPENED", db);
            this.db = db;
            resolve(db);
          },
          err => {
            console.log(`xDB | Database NOT OPENED: ${err}`);
            reject(err);
          }
        );
      };

      // Trinam duombazę jeigu egzistuoja
      if (useLocal === true) {
        // Netriname duombazės, naudojame (offline) duombazę.
        openDatabase();
      } else if (this.db) {
        SQLite.deleteDatabase(
          LOCAL_DB,
          () => {
            console.log(`xDB | ${LOCAL_DB} successfully deleted`, this.db);
            openDatabase();
          },
          () => {
            console.log(
              `xDB | ${LOCAL_DB} can't be deleted. Creating new one...`
            );
            openDatabase();
          }
        );
      } else {
        console.log(`xDB | ${LOCAL_DB} is not existing. Creating new one...`);
        openDatabase();
      }
    });
  }

  /** Parsiunčia duombazę iš remote šaltinio ir per naują inicializuoja
   * @return {Promise}
   */
  fetch() {
    return new Promise((resolve, reject) => {
      const { REMOTE_DB, REMOTE_ROOT } = this.SYNC;
      const uri = REMOTE_ROOT + REMOTE_DB;
      const dir = `${RNFetchBlob.fs.dirs.DocumentDir}/${REMOTE_DB}`; // /data/user/0/com.crud/files

      RNFetchBlob.config({
        path: dir,
        overwrite: true
      })
        .fetch("GET", uri)
        .then(res => {
          // console.log("The file saved to ", res.path());

          this.init()
            .then(db => {
              resolve(db);
            })
            .catch(err => {
              reject(err);
            });
        });
    });
  }

  pushRemote() {
    const { LOCAL_DB_URI, REMOTE_ROOT } = this.SYNC;

    const REMOTE_URI = `${REMOTE_ROOT}upload/upload.php`;

    // console.log(`${LOCAL_DB_URI} ---> ${REMOTE_URI}`);

    return new Promise((resolve, reject) => {
      RNFetchBlob.fetch(
        "POST",
        REMOTE_URI,
        {
          "Content-Type": "multipart/form-data"
        },
        [
          {
            name: DIRECTORIES.appDb,
            filename: DIRECTORIES.appDb,
            type: "application/x-sqlite3",
            data: RNFetchBlob.wrap(LOCAL_DB_URI)
          }
        ]
      )
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /** Parsiunčia duombazę iš remote šaltinio ir per naują inicializuoja
   * @param {string} statement SQL sakinys (SELECT * FROM Clients)
   * @param {array} args SQL sakinys (SELECT * FROM Clients)
   * @return {Promise} resolvina su SQL rezultatu
   */
  query<T>(statement, args = []): Promise<T[]> {
    return new Promise((resolve, reject) => {
      const initialState =
        this.db === undefined ? this.init() : Promise.resolve(this.db);
      initialState.then(
        (db: SQLiteDatabase) =>
          db.transaction(
            tx =>
              tx.executeSql(
                statement,
                args,
                (txs, results) => {
                  const len = results.rows.length;
                  const arr = [];

                  for (let i = 0; i < len; i++) {
                    const row = results.rows.item(i);
                    arr.push(row);
                  }

                  // console.log("xDB | Query results:", results, statement);
                  resolve({
                    data: arr,
                    response: results
                  });
                },
                err => {
                  debugger;
                  // console.log("xDB | Query error.", err);
                  reject(err);
                }
              ),
            err => {
              debugger;
              // console.log("xDB | Transaction error.", err);
              reject(err);
            }
          ),
        () => {
          debugger;
        }
      );
    });
  }

  /** Įkelia duombazę į remote serverį */
  push(): Promise {
    if (!this.db) return Promise.reject();

    const { LOCAL_DB_URI, REMOTE_ROOT } = this.SYNC;
    const REMOTE_URI = `${REMOTE_ROOT}upload/upload.php`;

    return new Promise((resolve, reject) => {
      RNFetchBlob.fetch(
        "POST",
        REMOTE_URI,
        {
          "Content-Type": "multipart/form-data"
        },
        [
          {
            data: RNFetchBlob.wrap(LOCAL_DB_URI),
            filename: DIRECTORIES.appDb,
            name: DIRECTORIES.appDb,
            type: "application/x-sqlite3"
          }
        ]
      )
        .then(res => {
          // console.log("xDB | Push response: ", res);
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}

/**
 * Escape'ina multi-line, kad galima būtų paduoti multi-line užklausas su ``.
 * @param {string} str
 * @return {string}
 */
export const sanitize = str =>
  str
    .replace(/[\r\n]/g, "") // returns
    .replace(/\s\s+/g, " ") // multiple spaces
    .replace(/^\s+|\s+$/g, " "); // trim

export function xquery<T>(
  statement: string,
  params = []
): Promise<DbResult<T>> {
  let database: SQLiteDatabase;
  let result: DbResult<T>;
  let dbName = DIRECTORIES.remoteDb;
  statement = sanitize(statement);

  const init = (): Promise<SQLiteDatabase, SQLError> => {
    return new Promise((resolve, reject) => {
      SQLite.openDatabase(
        {
          name: dbName, // lbs.db
          location: "Documents", // **/Documents/lbs.db
          createFromLocation: Platform.select({
            ios: "/" + dbName,
            android: "/" + dbName
          })
        },
        (db: SQLiteDatabase) => {
          // console.log("xDB | xquery | Database OPENED", db);
          database = db;
          resolve(db);
        },
        err => {
          // console.log("xDB | xquery | Database NOT OPENED", err);
          reject(err);
        }
      );
    });
  };

  const sql = (query, args) => {
    return new Promise((resolve, reject) => {
      console.log("xDB | xquery | Executing:", statement);
      database.executeSql(
        query,
        args,
        response => {
          const { rows } = response;
          const arr = [];

          for (let i = 0, len = rows.length; i < len; i++) {
            const row = rows.item(i);
            arr.push(row);
          }

          console.log("xDB | xquery | Query results:", arr);
          result = {
            data: arr,
            response
          };

          resolve(database);
        },
        err => reject(err)
      );
    });
  };

  const close = () => {
    return new Promise((resolve, reject) => {
      database.close(() => resolve(), err => reject(err));
    });
  };

  return (
    init()
      .then(_ => sql(statement, params))
      // .then(_ => close())
      .then(_ => result)
      .catch(err => {
        console.log("xquery | error |", err);
      })
  );
}

export function xrow<T>(statement: string, params = []): Promise<T> {
  return xquery(statement, params).then(result => {
    return result.data[0];
  });
}

// module.exports = new XDB();
