import { ResultSet } from "react-native-sqlite-storage";

export type LatLng = {
  lat: number,
  lng: number
};

export type MD5 = {
  md5: string,
  file: string,
  size: number
};

export type DbResult<T> = {
  data: T[],
  response: ResultSet
};

// TABLE QUERY OBJECTS
export type TObject = {
  AudioFilePath: string,
  Description: string,
  GardenId: number,
  IsBotany: number,
  IsHistoric: number,
  IsVisible: number,
  Latitude: number,
  Longitude: number,
  Name: string,
  ObjectId: number,
  PhotoUrl: string,
  RouteId: number,
  SortOrder: number,
  TypeId: number,
  VideoFilePath: string,

  DescriptionDe: string,
  DescriptionEn: string,
  DescriptionLv: string,
  DescriptionRu: string,

  IsVisibleDe: number,
  IsVisibleEn: number,
  IsVisibleLv: number,
  IsVisibleRu: number,

  NameDe: string,
  NameEn: string,
  NameLv: string,
  NameRu: string
};
