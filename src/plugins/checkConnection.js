import { NetInfo } from "react-native";
import RNFS from "react-native-fs";

/**
 * Funkcija, kuri patikrina kaip yra prisijungęs vartotojas. Jeigu yra prisijungęs, vykdomas fullyOnline cb, jeigu nėra prisijungęs, bet turintis failu tai partiallyOffline cb, o jeigu visiškai offline be jokių turimų duomenų - fullyOffline
 * @param {void} fullyOnline
 * @param {void} partiallyOffline
 * @param {void} fullyOffline
 */
export const checkConnection = (
  fullyOnline,
  partiallyOffline,
  fullyOffline
) => {
  console.log("checkConnection | Checking...");
  let online = false;
  let wifi = false;
  const localMD5Path = `${RNFS.DocumentDirectoryPath}/md5.json`;

  NetInfo.getConnectionInfo()
    .then(
      info => {
        online = info.type !== "none";
        wifi = info.type === "wifi";

        console.log("checkConnection | ", info);
        if (online === false) {
          return Promise.reject();
        }
        return Promise.resolve();
      },
      err => {
        // Nepavyko nustatyti ryšio
        console.error(err);
      }
    )
    .then(
      () => {
        // DEVICE ONLINE
        console.log("DEVICE IS ONLINE");
        if (typeof fullyOnline === "function") {
          fullyOnline();
        }
      },
      () => {
        // DEVICE OFFLINE
        console.log("DEVICE IS OFFLINE");
        RNFS.exists(localMD5Path).then(exists => {
          if (exists === true) {
            if (typeof partiallyOffline === "function") {
              partiallyOffline();
            }
          } else {
            if (typeof fullyOffline === "function") {
              fullyOffline();
            }
          }
        });
      }
    );
};
