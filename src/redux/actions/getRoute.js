import RNFS from "react-native-fs";
import { xquery, xrow } from "../../plugins/XDB";
import { GET_ROUTE } from "./types";

export default function getRoute(routeId) {
  return (dispatch, getState) => {
    const { locale } = getState().language;
    const { garden } = getState().garden;

    // ru -> Ru
    const suffix =
      locale === "lt"
        ? ""
        : locale.charAt(0).toUpperCase() + locale.substr(1).toLowerCase();

    const route$ = xrow(
      `
        SELECT
          RouteId,
          Name${suffix},
          Description${suffix},
          IsVisible${suffix},
          (
              SELECT ('${RNFS.DocumentDirectoryPath}' || Path) AS Path
              FROM Documents
              WHERE
              (
                refKey = 'RouteId'
                AND refVal = ${routeId}
              )
              ORDER BY TypeId DESC
              LIMIT 1
          ) AS MainPhoto
        FROM Routes
        WHERE RouteId = ${routeId} AND IsVisible = 1
      `
    );

    return Promise.all([objects$, route$])
      .then(results => {
        const routeObjects = results[0].data;
        const route = results[1];

        dispatch({
          type: GET_ROUTE,
          routeObjects,
          route
        });
      })
      .catch(err => console.log("GET DATA ERROR.", err));
  };
}
