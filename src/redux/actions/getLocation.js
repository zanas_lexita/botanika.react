import { GET_LOCATION } from "./types";
import { getCurrentPosition } from "../../plugins/Utilities";

export default function getLocation() {
  return (dispatch, getState) => {
    navigator.geolocation.getCurrentPosition(
      ({ coords }) => {
        dispatch({
          type: GET_LOCATION,
          lat: coords.latitude,
          lng: coords.longitude
        });
      },
      _ => console.log("Geolocation error.", _),
      {
        enableHighAccuracy: true,
        maximumAge: 60000,
        timeout: 20000,
        distanceFilter: 3
      }
    );
  };
}
