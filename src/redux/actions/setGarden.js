import { SET_GARDEN } from "./types";
import { KEYS } from "../../globals";
import { AsyncStorage } from "react-native";

export default function setGarden(gardenId) {
  return dispatch => {
    const set = (gId: number) => {
      dispatch({
        type: SET_GARDEN,
        garden: gId
      });
      return saveGarden(gId);
    };

    if (gardenId) {
      set(gardenId);
    } else {
      // Get current lang from AsyncStorage
      getGarden()
        .then(gId => set(gId))
        .catch(err => {
          console.warn(err);
          set();
        });
    }
  };
}

export function getGarden() {
  return AsyncStorage.getItem(KEYS.garden);
}

export function saveGarden(gardenId = "") {
  if (gardenId) {
    const msg = `Saving gardenId '${gardenId}' to AsyncStorage`;
    console.log(msg);
    return AsyncStorage.setItem(KEYS.garden, gardenId.toString()).then(_ => {
      console.log("DONE |", msg);
      return _;
    });
  }
}
