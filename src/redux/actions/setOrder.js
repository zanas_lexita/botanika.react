import { SET_ORDER, ORDER_BYS } from "./types";

export default function setOrder(arr) {
  return (dispatch, getState) => {
    const { order } = getState().order;

    const curIndex = ORDER_BYS.indexOf(order);
    const nextIndex = curIndex === ORDER_BYS.length - 1 ? 0 : curIndex + 1;
    const nextStatus = ORDER_BYS[nextIndex];

    dispatch({
      type: SET_ORDER,
      order: nextStatus
    });

    return nextStatus;
  };
}
