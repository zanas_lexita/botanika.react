import { DICTIONARY } from "../../plugins/translations";
import { SET_LANGUAGE } from "./types";
import { AsyncStorage } from "react-native";
import { KEYS } from "../../globals";

export default function setLanguage(loc) {
  return dispatch => {
    const set = locale => {
      locale = locale || "lt";
      dispatch({
        type: SET_LANGUAGE,
        lang: DICTIONARY(locale),
        locale
      });
      return saveLanguage(locale);
    };

    if (loc) {
      set(loc);
    } else {
      // Get current lang from AsyncStorage
      getLanguage()
        .then(l => set(l))
        .catch(err => {
          console.warn(err);
          set();
        });
    }
  };
}

function getLanguage() {
  return AsyncStorage.getItem(KEYS.language);
}

function saveLanguage(locale) {
  const msg = `Saving language '${locale}' to AsyncStorage`;
  console.log(msg);
  return AsyncStorage.setItem(KEYS.language, locale.toString()).then(_ => {
    console.log("DONE |", msg);
    return _;
  });
}
