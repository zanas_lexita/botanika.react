import { Platform } from "react-native";
import { GET_DATA } from "./types";
import { xquery, xrow } from "../../plugins/XDB";
import RNFS from "react-native-fs";
import {
  coordDistance,
  humanDistance,
  distanceDuration
} from "../../plugins/Utilities";

export default function getData() {
  return (dispatch, getState) => {
    const { locale } = getState().language;
    const { garden } = getState().garden;

    // ru -> Ru
    const suffix =
      locale === "lt"
        ? ""
        : locale.charAt(0).toUpperCase() + locale.substr(1).toLowerCase();

    const ImagePathCol = Platform.select({
      ios: `('${RNFS.DocumentDirectoryPath}' || Path) AS Path`,
      android: `('file://${RNFS.DocumentDirectoryPath}' || Path) AS Path`
    });
    const AudioPathCol = Platform.select({
      ios: "Path",
      android: `('${RNFS.DocumentDirectoryPath}' || Path) AS Path`
    });

    const objects$ = xquery(
      `
        SELECT
          o.ObjectId,
          o.Name${suffix ? suffix + " AS Name" : ""},
          o.IsVisible${suffix ? suffix + " AS IsVisible" : ""},
          o.Description${suffix ? suffix + " AS Description" : ""},
          o.Latitude,
          o.Longitude,
          o.TypeId,
          o.RouteId,
          o.IsBotany,
          o.IsHistoric,
          o.GardenId,
          (
            SELECT ${ImagePathCol}
            FROM Documents
            WHERE
            (
              TypeId IN('2', 'mainPhoto')
              AND refKey = 'ObjectId'
			        AND refVal = o.ObjectId
            )
            ORDER BY TypeId DESC
            LIMIT 1
          ) AS MainPhoto,
          (
            SELECT ${AudioPathCol}
            FROM Documents
            WHERE
            (
              TypeId = 'audio${suffix}'
              AND refKey = 'ObjectId'
			        AND refVal = o.ObjectId
            )
            ORDER BY TypeId DESC
            LIMIT 1
          ) AS MainAudio,
          od.Name${suffix} AS TypeName,
          od.Param1 AS TypeIcon
        FROM Objects o
        LEFT JOIN Options_Details od
        ON od.OptionDetailId = o.TypeId
        WHERE
        (
          GardenId = ?
          AND IsVisible = 1
          AND Latitude != ''
          AND Longitude != ''
		    )
      `,
      [garden]
    );

    const gardenInfo$ = xrow(
      `
        SELECT *
        FROM Gardens
        WHERE GardenId = ?
      `,
      [garden]
    );

    const types$ = xquery(
      `
        SELECT OptionDetailId, Name${suffix}, Param1 AS Icon
        FROM Options_Details
        WHERE OptionId = ?
      `,
      [3]
    );

    const routes$ = xquery(
      `
        SELECT
          RouteId,
          Name${suffix},
          Description${suffix},
          IsVisible${suffix},
          (
            SELECT ${ImagePathCol}
            FROM Documents
            WHERE
            (
              TypeId IN('mainPhoto')
              AND refKey = 'RouteId'
              AND refVal = r.RouteId
            )
            ORDER BY TypeId DESC
            LIMIT 1
          ) AS MainPhoto
        FROM Routes r
        WHERE GardenId = ? AND IsVisible = 1
      `,
      [garden]
    );

    return Promise.all([objects$, gardenInfo$, types$, routes$])
      .then(results => {
        const objects = results[0].data;
        const gardenInfo = results[1];
        const types = results[2].data;
        const routeData = results[3].data;

        const routesWithObjects$ = routeData.map(rd => {
          const rid = rd.RouteId;
          return xquery(
            `
              SELECT
                o.ObjectId,
                o.Name${suffix ? suffix + " AS Name" : ""},
                o.IsVisible${suffix ? suffix + " AS IsVisible" : ""},
                o.Description${suffix ? suffix + " AS Description" : ""},
                o.Latitude,
                o.Longitude,
                o.TypeId,
                o.RouteId,
                o.IsBotany,
                o.IsHistoric,
                o.GardenId,
                (
                  SELECT ${ImagePathCol}
                  FROM Documents
                  WHERE
                  (
                    TypeId IN('2', 'mainPhoto')
                    AND refKey = 'ObjectId'
                    AND refVal = o.ObjectId
                  )
                  ORDER BY TypeId DESC
                  LIMIT 1
                ) AS MainPhoto,
                (
                  SELECT ${AudioPathCol}
                  FROM Documents
                  WHERE
                  (
                    TypeId = 'audio${suffix}'
                    AND refKey = 'ObjectId'
                    AND refVal = o.ObjectId
                  )
                  ORDER BY TypeId DESC
                  LIMIT 1
                ) AS MainAudio,
                od.Name${suffix} AS TypeName,
                od.Param1 AS TypeIcon
              FROM Objects o
              LEFT JOIN Options_Details od
              ON od.OptionDetailId = o.TypeId
              WHERE
              (
                GardenId = ${garden}
                AND RouteId = ${rid}
                AND IsVisible = 1
                AND Latitude != ''
                AND Longitude != ''
              )
              ORDER BY o.SortOrder
            `
          )
            .then(response => ({ ...rd, objects: response.data }))
            .then(routeWithObjects => {
              // calculate object distance
              const objects = routeWithObjects.objects;
              let distance = 0;

              for (let i = 1, j = i - 1; i < objects.length; i++, j++) {
                if (objects[i]) {
                  distance += coordDistance(
                    {
                      lat: objects[i].Latitude,
                      lng: objects[i].Longitude
                    },
                    {
                      lat: objects[j].Latitude,
                      lng: objects[j].Longitude
                    }
                  );
                }
              }

              const routeWithDistance = {
                ...routeWithObjects,
                distance: humanDistance(distance),
                duration: distanceDuration(distance)
              };

              return routeWithDistance;
            });
        });

        Promise.all(routesWithObjects$).then(routesWithObjects => {
          dispatch({
            type: GET_DATA,
            objects,
            gardenInfo,
            types,
            routes: routesWithObjects
          });
        });
      })
      .catch(err => console.log("GET DATA ERROR.", err));
  };
}
