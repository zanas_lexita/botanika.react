import { Geolocation } from "react-native";
import { WATCH_LOCATION } from "./types";
import { coordDistance } from "../../plugins/Utilities";

export default function watchLocation() {
  return (dispatch, getState) => {
    navigator.geolocation.stopObserving();
    navigator.geolocation.watchPosition(
      ({ coords }) => {
        const newPosition = {
          lat: coords.latitude,
          lng: coords.longitude
        };

        const callReducer = () => {
          dispatch({
            type: WATCH_LOCATION,
            ...newPosition
          });
        };

        const { location } = getState().location;

        if (!location) {
          callReducer();
        } else if (coordDistance(newPosition, location) > 5) {
          callReducer();
        } else {
          console.log(
            "Too close location update. Distance: " +
              coordDistance(newPosition, location),
            newPosition,
            location
          );
        }
      },
      err => console.log("Location error", err),
      {
        timeout: 20000,
        maximumAge: 15000,
        enableHighAccuracy: true,
        distanceFilter: 2
      }
    );
  };
}
