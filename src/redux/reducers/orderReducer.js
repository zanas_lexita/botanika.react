import { SET_ORDER, ORDER_BYS } from "./../actions/types";

const initialState = {
  order: ORDER_BYS[0]
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_ORDER:
      return {
        ...state,
        order: action.order
      };
    default:
      return state;
  }
}
