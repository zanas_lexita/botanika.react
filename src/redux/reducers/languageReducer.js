import { DICTIONARY } from "../../plugins/translations";
import { SET_LANGUAGE } from "./../actions/types";

const initialState = {
  locale: "lt",
  lang: DICTIONARY("lt")
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_LANGUAGE:
      return {
        ...state,
        locale: action.locale,
        lang: action.lang
      };
    default:
      return state;
  }
}
