import { GET_DATA, GET_ROUTE } from "./../actions/types";

const initialState = {
  gardenInfo: {},
  objects: [],
  types: [],
  routes: [],
  route: [],
  routeId: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_DATA:
      return {
        ...state,
        gardenInfo: action.gardenInfo,
        types: action.types,
        objects: action.objects,
        routes: action.routes
      };
    case GET_ROUTE:
      return {
        ...state,
        route: action.route,
        routeId: action.routeId
      };
    default:
      return state;
  }
}
