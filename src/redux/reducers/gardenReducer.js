import { SET_GARDEN } from "./../actions/types";

const initialState = {
  garden: undefined
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_GARDEN:
      return {
        ...state,
        garden: action.garden
      };
    default:
      return state;
  }
}
