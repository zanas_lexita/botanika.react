import { combineReducers } from "redux";
import languageReducer from "./languageReducer";
import gardenReducer from "./gardenReducer";
import fetchReducer from "./fetchReducer";
import locationReducer from "./locationReducer";
import orderReducer from "./orderReducer";

export default combineReducers({
  language: languageReducer,
  location: locationReducer,
  garden: gardenReducer,
  fetch: fetchReducer,
  order: orderReducer
});
