import { GET_LOCATION, WATCH_LOCATION } from "./../actions/types";

const initialState = {
  location: undefined
};

export default function(state = initialState, action) {
  switch (action.type) {
    case WATCH_LOCATION:
    case GET_LOCATION:
      return {
        ...state,
        location: {
          lat: action.lat,
          lng: action.lng
        }
      };
    default:
      return state;
  }
}
