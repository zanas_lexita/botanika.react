import { Platform } from "react-native";
import { applyMiddleware, compose, createStore } from "redux";
import logger from "redux-logger";
import promise from "redux-promise";
import thunk from "redux-thunk";
import devTools from "remote-redux-devtools";
import RootReducer from "./reducers";

const middleware = applyMiddleware(thunk, promise, logger);

const Store = createStore(
  RootReducer,
  compose(
    middleware,
    devTools({
      name: Platform.OS,
      hostname: "localhost",
      port: 5678
    })
  )
);

export default Store;
